const port = require('../config.js');

function payFor(token, uid,body, mealType, cb) {
  wx.request({
    url: port.payFor, //请求地址
    header: { //请求头
      "Content-Type": "applciation/json",
      "Authorization": "Bearer " + token
    },
    data: {
      uid: uid,
      body: body,
      goods: mealType
    },
    method: "post", //get为默认方法/POST
    success: function(res) {
      if (res.data.status == 'success') {
        var data = new Object();
        data.appId = res.data.data.appId;
        data.nonceStr = res.data.data.nonceStr;
        data.package = res.data.data.package;
        data.paySign = res.data.data.paySign;
        data.signType = res.data.data.signType;
        data.timeStamp = res.data.data.timeStamp;
        return typeof cb == "function" && cb(data)
      } else {
        return typeof cb == "function" && cb('')
      }
    },
    fail: function(err) {}, //请求失败
    complete: function(suc) { //请求完成后执行的函数
    }
  })
}

module.exports.payFor = payFor;