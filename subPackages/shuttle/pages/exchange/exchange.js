// pages/exchange/exchange.js
var app = getApp();
const port = require('../../../../config.js');
const shuttle = require('../shuttle.js');
const judgeVip = require('../../../../common/judgeVip.js');


Page({

  /**
   * 页面的初始数据
   */
  data: {
    num: 3,
    animationData: {},
    idI: 1,
    MyCard2: true,
    MyCard3: true,
    changeShow:false,
    shuttleMessage: shuttle.shuttleMessage,  //跳转分包message统一路径
    shuttleMyCenter: shuttle.shuttleMyCenter,  //跳转分包myCenter统一路径
    shuttleIndex: shuttle.shuttleIndex,   //跳转主包统一路径
    vlogo: false    //合伙人logo
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.hideShareMenu();
    var that = this;
    app.getUserInfo(function (personInfo) {
      var token = personInfo.token;
      var uid = personInfo.uid;
      var openid = personInfo.openId;
      //获取交换信息
      wx.request({
        url: port.applylist,//请求地址
        header: {//请求头
          "Content-Type": "applciation/json",
          "Authorization": "Bearer " + token
        },
        data: { 'openid': openid },
        method: "post",//get为默认方法/POST
        success: function (res) {
          if(res.data.data.length==0){
            that.setData({
              changeShow: true,
              MyCard2:false,
              MyCard3:false
            })
          }else{
            if (res.data.status == 'success') {
              var dataLen = res.data.data.length;
              var kaMsg = new Array()
              if (dataLen > 0) {
                if (dataLen == 1) {
                  that.setData({
                    MyCard2: false,
                    MyCard3: false
                  })
                } else if (dataLen == 2) {
                  that.setData({
                    MyCard2: false,
                  })
                }
                for (var i = 0; i < dataLen; i++) {
                  var kaTemp = new Object();
                  //基本属性
                  kaTemp.wheShow = 'none';
                  kaTemp.id = i + 1;
                  kaTemp.wheAnimate = 'ChangeMyCard1';
                  kaTemp.animationData = '';
                  kaTemp.zIndex = '3';

                  //用户信息属性
                  kaTemp.is_vip = res.data.data[i].is_vip
                  kaTemp.userId = res.data.data[i].id;
                  kaTemp.uid = res.data.data[i].uid;
                  kaTemp.plus_uid = res.data.data[i].plus_uid;
                  kaTemp.cardid = res.data.data[i].cardid;
                  kaTemp.plus_cardid = res.data.data[i].plus_cardid;
                  kaTemp.apply_content = res.data.data[i].apply_content;
                  kaTemp.created_at = res.data.data[i].created_at;
                  kaTemp.updated_at = res.data.data[i].updated_at;
                  kaTemp.company = res.data.data[i].company;
                  kaTemp.duty = res.data.data[i].duty;
                  kaTemp.username = res.data.data[i].username;
                  kaTemp.mobile = res.data.data[i].mobile;
                  kaTemp.mailbox = res.data.data[i].mailbox;
                  kaTemp.avatar = port.imgPath +res.data.data[i].avatar;
                  kaTemp.address = res.data.data[i].ssq + res.data.data[i].address;
                  kaTemp.status = res.data.data[i].status;
                  if (res.data.data[i].apply_content == "" || res.data.data[i].apply_content == null){
                    res.data.data[i].apply_content = '您好，请问可以交个朋友吗？很想认识您这个朋友。';
                  }
                  kaTemp.apply_content = res.data.data[i].apply_content;
                  kaMsg.push(kaTemp);
                }
                kaMsg[0].wheShow = "block";
                that.setData({
                  kaMsg: kaMsg
                })
              } else {
                that.setData({
                  MyCard2: false,
                  MyCard3: false
                })
              }

            }
          }
         
        },
        fail: function (err) {
          wx.hideLoading()
        },//请求失败
        complete: function (suc) {//请求完成后执行的函数
        }
      })

    })
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  translateMove: function (numId) {
    var that = this;
    var numId = numId;
    var numLength = this.data.kaMsg.length;
    var num = numLength - numId;
    var animation = wx.createAnimation({
      duration: 3000,
      timingFunction: 'ease',
      delay: 0,
      transformOrigin: '"50% 50% 0"',
    })
    this.animation = animation;
    this.animation.translate(-1000, 0).step()
    var kaMsg = new Array();
    kaMsg = this.data.kaMsg;
    this.setData({
      animationData: this.animation.export()
    })
    kaMsg[numId - 1].animationData = this.data.animationData;
    kaMsg[numId - 1].zIndex = '9';

    this.setData({
      kaMsg: kaMsg,
      num: num,
    });
    var kaMsg = new Array();
    kaMsg = this.data.kaMsg;

    if (numId < kaMsg.length)
      kaMsg[numId].wheShow = "block";
    // kaMsg[numId - 1].wheShow = "none"
    this.setData({
      kaMsg: kaMsg
    })

  },
  shenqingSubmit: function (e) {
    var formId = e.detail.formId;
    var apply_id = e.detail.target.dataset.user_id;
    var kind = e.detail.target.dataset.kind;
    var numId = e.detail.target.dataset.this_id;
    this.kindRequest(formId, apply_id, kind, numId);
  },
  kindRequest: function (formId, apply_id, kind, numId) {
    var that = this;
    var formId = formId;
    var apply_id = apply_id;
    var status = kind;
    var numId = numId;
    app.getUserInfo(function (personInfo) {
      var token = personInfo.token;
      var uid = personInfo.uid;
      var openid = personInfo.openId;
      // 获取交换信息
      wx.request({
        url: port.CardSwap,//请求地址
        header: {//请求头
          "Content-Type": "applciation/json",
          "Authorization": "Bearer " + token
        },
        data: { 'apply_id': apply_id, 'status': status, 'form_id': formId },
        method: "post",//get为默认方法/POST
        success: function (res) {
          if (res.data.status == 'success') {
            wx.showToast({
              title: res.data.message,
              mask: true,
              duration: 2000,
              success: function (r) {
              }
            })
            that.setData({
              MyCard2: false,
              MyCard3: false,
            })
            that.onLoad();
            // that.translateMove(numId); 
          }else{
            wx.showToast({
              title: res.data.message,
              mask: true,
              duration: 2000,
              success: function (r) {
                that.onLoad();  
              }
            })
          }
        },
        fail: function (err) {
          wx.hideLoading()


        },//请求失败
        complete: function (suc) {//请求完成后执行的函数

        }
      })
    })
  }
})