var app = getApp();
const port = require('../../../../config.js');
const center = require('../center.js');

var datalist = function (that, condition) {
  app.getUserInfo(function (personInfo) {
    var token = personInfo.token;
    var uid = personInfo.uid;
    wx.request({
      url: port.CardBrowseRecord,//请求地址
      header: {//请求头
        "Content-Type": "applciation/json",
        "Authorization": "Bearer " + token
      },
      data: { 'uid': uid, 'status': 2, 'limit': condition.limit, 'date': condition.date },
      method: "post",//get为默认方法/POST
      success: function (res) {
        if (res.data.status == 'success') {
          if (res.data.data.length == 0) {
            that.setData({
              changeShow: true
            })
          } else {
            for (var i in res.data.data) {
              var dataFirst = res.data.data[i]
              for (var j = 0; j <= dataFirst.length; j++) {
                var dataSecond = dataFirst[j]
                for (var s in dataSecond) {
                  if (dataSecond[s] == null || dataSecond[s] == "null") {
                    dataSecond[s] = "";
                  }
                }
              }
            }
            that.setData({
              cardall: res.data.data,
              count: res.data.count,
              allcount: res.data.allcount,
            })
          }
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none',
            mask: true,
            duration: 2000,
            success: function () {
              setTimeout(function () {
                that.history()
              }, 2000)
            }
          })
        }
      },
      fail: function (err) { },//请求失败
      complete: function (suc) {//请求完成后执行的函数
      }
    })
  })
};
Page({
  data: {
    cardall: [],
    date: '',
    start: '',
    end: '',
    imgPath: port.imgPath,
    limit: 1,
    count: 0,
    allcount: 0,
    changeShow: false, //是否显示无数据图标
    centerShuttle: center.centerShuttle,   //跳转分包shuttle统一路径
    centerMessage: center.centerMessage,  //跳转分包message统一路径
    centerIndex: center.centerIndex,   //跳转主包统一路径
    vlogo: false, //企业logo
  },

  download: function () {
    var that = this;
    if (that.data.count != that.data.allcount) {
      wx.showToast({
        title: '加载中...',
        icon: 'loading',
        mask: true,
      })
      that.setData({
        limit: that.data.limit + 1
      })
      var condition = { limit: that.data.limit, date: that.data.date };
      datalist(that, condition)
    }
  },

  history: function () {
    wx.navigateBack({
      delta: 1
    })
  },
  download: function () {
    var that = this;
    if (that.data.count != that.data.allcount) {
      wx.showToast({
        title: '加载中...',
        icon: 'loading',
        mask: true,
      })
      that.setData({
        limit: that.data.limit + 1
      })
      var condition = { limit: that.data.limit, date: that.data.date };
      datalist(that, condition)
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.hideShareMenu();
    var that = this;
    //当前时间
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth() + 1
    var day = date.getDate();
    var end = year + '-' + month + '-' + day;
    //3个月前的时间
    var ndate = new Date(Date.parse(date) - 7776000000);
    var nyear = ndate.getFullYear();
    var nmonth = ndate.getMonth() + 1
    var nday = ndate.getDate();
    var start = nyear + '-' + nmonth + '-' + nday;

    that.setData({
      start: start,
      end: end
    })
    var condition = { limit: that.data.limit, date: that.data.date };
    datalist(that, condition)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})