var app = getApp();
const port = require('../../../../config.js');
const messageCheck = require('../permessage/messageCheck.js')
const message = require('../message.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    dataId: 1,
    tempFilePaths: [],
    region: [],
    card: [],
    tximg: '/images/jimo.png',
    avatar: '',
    phone: 0,
    t: true,
    imgPath: port.imgPath,
    address: '',
    messageShuttle: message.messageShuttle,   //跳转分包library统一路径
    messageCenter: message.messageCenter,  //跳转分包myCenter统一路径
    messageIndex: message.messageIndex   //跳转主包统一路径
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.hideShareMenu();
    var that = this;
    //用户数据
    var mobile = options['手机'].split('-').join('');
    var address;
    if (options['地址']) {
      address = options['地址'];
    } else {
      address = options['英文地址'];
    }
    that.setData({
      card: options,
      mobile: mobile,
      address: address,
    })
  },

  //基本信息表单提交
  jibenMsgSubmit: function (e) {
    var that = this;
    var datas = '';
    var username = e.detail.value.username.trim(); //姓名
    var mobile = e.detail.value.mobile; //手机
    var company = e.detail.value.company.trim(); //公司
    var duty = e.detail.value.duty.trim(); //职务
    var mailbox = e.detail.value.mailbox.trim(); //邮箱
    var ssq = e.detail.value.ssq; //省市区
    var address = e.detail.value.address.trim(); //详细地址
    var types = 1;
    var url = '';
    var avatar = that.data.avatar;
    var code='';
    var codeShow='';
    //判断表单验证通过
    var result = messageCheck.messageCheck(avatar, username, mobile, code, codeShow, company, duty, address, ssq, port.imgPath);
    if (result == false) {
      return
    }
    //判断头像是否为空
    if (!avatar) {
      avatar = 'new-background/logo_aboutus.png';
    }
    that.setData({
      t: false
    })
    wx.showLoading({
      title: '正在创建',
    })
    //设置默认咖片样式
    var templet_sz = new Object();
    templet_sz.smallBg = port.imgPath + "new-background/newbg-2.png";
    templet_sz.bigBg = port.imgPath + "new-background/newbg-2long.png";
    // templet_sz.iconColor="white";
    templet_sz.bgImgColor = "white";
    templet_sz.id_smallbg = "0";

    //发送请求
    app.getUserInfo(function (personInfo) {
      var token = personInfo.token;
      var uid = personInfo.uid;
      datas = {
        'uid': uid,
        'username': username,
        'mobile': mobile,
        'company': company,
        'duty': duty,
        'email': mailbox,
        'ssq': ssq,
        'address': address,
        'types': types,
        'avatar': avatar,
        'templet_sz': templet_sz,
      }
      wx.request({
        url: port.papercreate,//请求地址
        header: {//请求头
          "Content-Type": "applciation/json",
          "Authorization": "Bearer " + token
        },
        data: datas,
        method: "post",//get为默认方法/POST
        success: function (res) {
          wx.hideToast();
          var messageIndex = that.data.messageIndex;
          wx.reLaunch({
            url: messageIndex + '/cardHolder',
          })
        },
        fail: function (err) {
          wx.hideLoading()
        },//请求失败
        complete: function (suc) {//请求完成后执行的函数
        }
      })

    })
  },

  //监听电话号码变化
  mobileInput: function (e) {
    var that = this;
    that.setData({
      phone: e.detail.value
    })
  },

  back: function () {
    var that = this;
    wx.navigateBack({
      delta: 1
    })
  },

  //地址选择
  bindRegionChange: function (e) {
    this.setData({
      region: e.detail.value
    })
  },
  //这里是选取图片的方法
  choose: function () {
    var that = this;
    wx.chooseImage({
      count: 1,
      success: function (res) {
        var tempFilePaths = res.tempFilePaths
        that.setData({
          tximg: tempFilePaths[0],
          avatar: tempFilePaths[0],
        })
      },
      faill: function (res) {
      }
    })

  },
  //获取当前位置
  getLocation: function () {
    var that = this;
    wx.chooseLocation({
      success: function (res) {
        that.setData({
          address: res.name
        })
      },
    })
  },

  getuserinfo: function (e) {
    var that = this;
    var result = e.detail.errMsg;
    if (result == 'getUserInfo:ok') {
      var tximg = e.detail.userInfo.avatarUrl;
      that.setData({
        tximg: tximg
      })
    }
    app.getUserInfo(function (personInfo) {
      var token = personInfo.token;
      var uid = personInfo.uid;
      var userinfo = new Array();
      userinfo = e.detail.userInfo;

      wx.request({
        url: port.setuserinfo,//请求地址
        header: {//请求头
          "Content-Type": "applciation/json",
          "Authorization": "Bearer " + token
        },
        data: { 'userinfo': userinfo, 'uid': uid },
        method: "post",//get为默认方法/POST
        success: function (res) {
          if (res.data.status == 'success') {

          } else {
            that.setData({
              t: true
            })
            wx.showToast({
              title: res.data.message,
              icon: 'none',
              mask: true,
              duration: 1500
            })
          }
        },
        fail: function (err) { },//请求失败
        complete: function (suc) {//请求完成后执行的函数

        }
      })
    })

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function (res) {
  },

  bindInputBlur: function (e) {
    this.inputValue = e.detail.value
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function (options) {
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})