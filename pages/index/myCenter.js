var app = getApp();
const port = require('../../config.js');
const mainIndex = require('../index.js');
const judgeVip = require('../../common/judgeVip.js');

Page({
  /**
   * 页面的初始数据
   */
  data: {
    username: '', //用户姓名
    duty: '', //用户职业
    company: '', //用户公司
    imgPath: port.imgPath,
    avatar: '', //用户头像
    integral: 0, //用户积分
    signShow: true, //是否签到
    continuous: 0, //连续签到次数
    showModal: false, //签到页面
    one: true, //一次签到
    oneimg: '/imgs/one.png',
    two: false, //连续签到
    twoimg: '/imgs/two.png',
    margin: '0', //进度条
    left: '12', //天数距离左边
    cid: '', //用户id
    qdbg: port.imgPath + 'new-background/qdbg.png',
    showRules: false,
    securityValue: '安全中心',
    centerCheck: 1, //判断第一次进入
    prompt: 1,
    indexShuttle: mainIndex.indexShuttle, //跳转分包shuttle统一路径
    indexMessage: mainIndex.indexMessage, //跳转分包message统一路径
    indexCenter: mainIndex.indexCenter, //跳转分包center统一路径
    securityIndex: 0, //安全中心层级
    is_vip: false, //是否是会员
    vipTip: false, //成为会员弹窗
    vlogo: false, //合伙人logo
    newview: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    wx.hideShareMenu();
    var that = this;
    app.getUserInfo(function(personInfo) {
      var token = personInfo.token;
      var uid = personInfo.uid;
      //请求个人信息
      wx.request({
        url: port.Personal, //请求地址
        header: { //请求头
          "Content-Type": "applciation/json",
          "Authorization": "Bearer " + token
        },
        data: {
          uid: uid
        },
        method: "post", //get为默认方法/POST
        success: function(res) {
          if (res.data.status == 'success') {
            if (res.data.data1 != null && res.data.data1 != '') {
              if (res.data.data1.duty == null) {
                res.data.data1.duty = "";
              }
              that.setData({
                username: res.data.data1.username,
                avatar: res.data.data1.avatar,
                duty: res.data.data1.duty,
                company: res.data.data1.company,
                cid: res.data.data1.id,

              })
            } else {
              that.setData({
                username: res.data.data2.nickname,
                avatar: res.data.data2.avatar,
              })
            }
            if (res.data.data3 != null && res.data.data3 != '') {
              var date = new Date();
              var year = date.getFullYear()
              var month = date.getMonth() + 1
              if (month < 10) {
                month = '0' + month;
              };
              var day = date.getDate();
              if (day < 10) {
                day = '0' + day;
              };
              var time = year + '' + month + '' + day;
              if (time == res.data.data3.time) {
                that.setData({
                  signShow: false
                })
              }
              if (time - res.data.data3.time < 2) {
                if (res.data.data3.continuous >= 7) {
                  that.setData({
                    continuous: res.data.data3.continuous,
                    left: "84",
                    margin: "98",
                    one: false,
                    two: true
                  })
                } else {
                  that.setData({
                    continuous: res.data.data3.continuous,
                    left: res.data.data3.continuous * 12,
                    margin: res.data.data3.continuous * 14,
                    one: true,
                    two: false
                  })
                }
              }
            }
            that.setData({
              integral: res.data.data2.integral,
            })
          } else {
            wx.showToast({
              title: res.data.message,
              icon: 'none',
              mask: true,
              duration: 1500
            })
          }
        },
        fail: function(err) {}, //请求失败
        complete: function(suc) { //请求完成后执行的函数
        }
      })

      //判断是否已绑定账号
      wx.request({
        url: port.checkuserinfo, //请求地址
        header: { //请求头
          "Content-Type": "applciation/json",
          "Authorization": "Bearer " + token
        },
        data: {
          user: uid
        },
        method: "post", //get为默认方法/POST
        success: function(res) {
          //is_pass 未绑定：0；绑定：1
          if (res.data.status == 'success') {
            var is_pass = res.data.data.is_pass;
            if (is_pass == 1) {
              that.setData({
                securityValue: '修改密码',
                centerCheck: 1,
              })
              wx.setStorage({
                key: 'centerCheck',
                data: 1,
              })
            } else if (is_pass == 0) {
              //判断是否首次进入
              var cardCheck = wx.getStorage({
                key: 'centerCheck',
                success: function(res) {
                  if (res.data == 0) {
                    that.setData({
                      centerCheck: 0,
                      securityIndex: 99
                    })
                  } else if (res.data == 1) {
                    that.setData({
                      centerCheck: 1,
                    })
                  }
                },
              })
            }
            if (res.data.data.is_browserecord > 0){
              that.setData({
                newview: true
              })
            }
          } else {
            wx.showToast({
              title: res.data.message,
              icon: 'none',
              mask: true,
              duration: 1500
            })
          }
        },
        fail: function(err) {}, //请求失败
        complete: function(suc) { //请求完成后执行的函数
        }
      })

      //判断会员
      judgeVip.judgeVip(token, uid, function(result) {
        if (result) {
          that.setData({
            is_vip: true
          })
        } else {
          that.setData({
            is_vip: false
          })
        }
      })
    })
  },

  //跳转会员中心
  vipCenter: function() {
    var that = this;
    var cid = that.data.cid
    if (!cid) {
      wx.showToast({
        icon: 'success',
        image: '/images/error.png',
        title: '请先添加名片',
      })
    } else {
      var indexShuttle = that.data.indexShuttle;
      wx.navigateTo({
        url: indexShuttle + '/vipCenter/vipCenter',
      })
    }
  },
  //关闭成为vip提示
  cancelVipTip: function() {
    var that = this;
    that.setData({
      vipTip: false
    })
  },


  //下一步引导
  nextPrompt: function(e) {
    var that = this;
    var prompt = e.currentTarget.dataset.prompt;
    if (prompt == 1) {
      wx.setStorage({
        key: 'centerCheck',
        data: 1,
      })
    }
    that.setData({
      prompt: prompt
    })
    var centerCheck = wx.getStorage({
      key: 'centerCheck',
      success: function(res) {
        if (res.data == 0) {
          that.setData({
            centerCheck: 0,
          })
        } else if (res.data == 1) {
          that.setData({
            centerCheck: 1,
            securityIndex: 0,
          })
        }
      },
    })
  },

  //显示积分规则
  showRules: function() {
    this.setData({
      showRules: true,
    })
  },

  // 头像放大
  headerImg: function() {
    var that = this;
    var imgs = that.data.avatar
    wx.previewImage({
      urls: [that.data.imgPath + imgs], // 需要预览的图片http链接列表
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.onLoad();
  },
  sign: function() {
    var that = this;
    app.getUserInfo(function(personInfo) {
      var token = personInfo.token;
      var uid = personInfo.uid;
      wx.request({
        url: port.Cardsign, //请求地址
        header: { //请求头
          "Content-Type": "applciation/json",
          "Authorization": "Bearer " + token
        },
        data: {
          uid: uid
        },
        method: "post", //get为默认方法/POST
        success: function(res) {
          if (res.data.status == 'success') {
            that.onLoad();
          } else {
            wx.showToast({
              title: res.data.message,
              icon: 'none',
              mask: true,
              duration: 1500
            })
          }
        },
        fail: function(err) {}, //请求失败
        complete: function(suc) { //请求完成后执行的函数
        }
      })
    })
  },
  // 弹出层
  showDialogBtn: function() {
    var cid = this.data.cid
    if (!cid) {
      wx.showToast({
        icon: 'success',
        image: '/images/error.png',
        title: '请先添加名片',
      })
    } else {
      this.setData({
        showModal: true
      })
    }
  },
  goRanking: function() {
    var that = this;
    var indexCenter = that.data.indexCenter;
    wx.navigateTo({
      url: indexCenter + '/rankingList/rankingList',
    })
  },
  goPDetails: function() {
    var that = this;
    var indexCenter = that.data.indexCenter;
    wx.navigateTo({
      url: indexCenter + '/pointsDetail/pointsDetail',
    })
  },
  /**
   * 弹出框蒙层截断touchmove事件
   */
  preventTouchMove: function() {

  },
  /**
   * 隐藏模态对话框
   */
  hideModal: function() {
    this.setData({
      showModal: false,
      showRules: false,
    });
  },


  //我的邀请
  myInvite: function() {
    var that = this;
    var indexCenter = that.data.indexCenter;
    wx.navigateTo({
      url: indexCenter + '/myInvite/myInvite',
    })
  },
  //我看过谁的
  browse: function() {
    var cid = this.data.cid
    if (!cid) {
      wx.showToast({
        icon: 'success',
        image: '/images/error.png',
        title: '请先添加名片',
      })
    } else {
      var that = this;
      var indexCenter = that.data.indexCenter;
      wx.navigateTo({
        url: indexCenter + '/browse/browse',
      })
    }
  },
  //谁看过我的
  whosee: function() {
    var that = this;
    var cid = this.data.cid
    if (!cid) {
      wx.showToast({
        icon: 'success',
        image: '/images/error.png',
        title: '请先添加名片',
      })
    } else {
      if (that.data.is_vip == true) {
        var indexCenter = that.data.indexCenter;
        wx.navigateTo({
          url: indexCenter + '/whosee/whosee',
        })
      } else {
        that.setData({
          vipTip: true
        })
      }

    }
  },
  //关于我们
  aboutus: function() {
    var that = this;
    var indexCenter = that.data.indexCenter;
    wx.navigateTo({
      url: indexCenter + '/aboutus/aboutus',
    })
  },
  //帮助中心
  helpCenter: function() {
    var that = this;
    var indexCenter = that.data.indexCenter;
    wx.navigateTo({
      url: indexCenter + '/helpCenter/helpCenter',
    })
  },
  //安全中心
  securityCenter: function() {
    var that = this;
    var indexCenter = that.data.indexCenter;
    wx.navigateTo({
      url: indexCenter + '/securityCenter/securityCenter',
    })
  },
  //联系我们
  contactus: function() {
    var that = this;
    var indexCenter = that.data.indexCenter;
    wx.navigateTo({
      url: indexCenter + '/contactus/contactus',
    })
  },
  //积分商城
  pointsShop: function() {
    wx.showToast({
      title: '一大波商品正在路上，敬请期待哦~',
      icon: 'none'
    })
    // var that = this;
    // var indexShuttle = that.data.indexShuttle;
    // wx.navigateTo({
    //   url: indexShuttle + '/shoppingMail/shoppingMail',
    // })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

})