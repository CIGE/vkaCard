var app = getApp();
const port = require('../../../../config.js');
const center = require('../center.js');

var interval = null //倒计时函数
Page({
  data: {
    phoneNo: '',
    idCode: '',
    password: '',
    passwordAlert: false,
    passwordCon: '',
    passwordconAlert: false,
    wheInput: false,
    time: '获取验证码', //倒计时 
    currentTime: 60,
    username: '', //账户名默认显示
    initailPass: '密码',
    initailUser: '账户名(设置之后不可更改)',
    passwordY: '',
    uid:"",
    centerShuttle: center.centerShuttle,   //跳转分包shuttle统一路径
    centerMessage: center.centerMessage,  //跳转分包message统一路径
    centerIndex: center.centerIndex   //跳转主包统一路径
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.hideShareMenu();
    var that = this;
    app.getUserInfo(function (personInfo) {
      var token = personInfo.token;
      var uid = personInfo.uid;
      that.setData({
        uid: uid
      })
      //请求个人信息
      wx.request({
        url: port.Personal,//请求地址
        header: {//请求头
          "Content-Type": "applciation/json",
          "Authorization": "Bearer " + token
        },
        data: { uid: uid },
        method: "post",//get为默认方法/POST
        success: function (res) {
          that.setData({
            phoneNo: res.data.data1.mobile
          })
        },
        fail: function (err) { },//请求失败
        complete: function (suc) {//请求完成后执行的函数
        }
      })
      //判断是否已绑定账号
      wx.request({
        url: port.checkuserinfo,//请求地址
        header: {//请求头
          "Content-Type": "applciation/json",
          "Authorization": "Bearer " + token
        },
        data: { user: uid },
        method: "post",//get为默认方法/POST
        success: function (res) {
          //is_pass 未绑定：0；绑定：1
          if (res.data.status == 'success') {
            var is_pass = res.data.data.is_pass;
            if(is_pass == 1){
              var username = res.data.data.name;
              that.setData({
                wheInput: true,
                username: username,
                initailPass: '请输入新密码'
              })
            }
          } else {
            wx.showToast({
              title: res.data.message,
              icon: 'none',
              mask: true,
              duration: 1500
            })
          }
        },
        fail: function (err) { },//请求失败
        complete: function (suc) {//请求完成后执行的函数
        }
      })
    })
  },

  //获取验证码
  getVerificationCode() {
    var that = this
    var phone = that.data.phoneNo;
    if (phone.length != 11) {
      wx.showToast({
        title: '请正确输入手机号',
        image: '/imgs/error.png',
        duration: 1500
      })
      return false;
    }
    app.getUserInfo(function (personInfo) {
      var token = personInfo.token;
      wx.request({
        url: port.CodeSMS,//请求地址
        header: {//请求头
          "Content-Type": "applciation/json",
          "Authorization": "Bearer " + token
        },
        data: {
          mobile: phone,
          uid: personInfo.uid
        },
        method: "post",//get为默认方法/POST
        success: function (res) {
          if (res.data.status == 'success') {
            wx.showToast({
              title: '发送成功',
              icon: 'success',
              duration: 1500
            })
            that.setData({
              disabled: true
            })
            that.getCode();
          } else {
            wx.showToast({
              title: res.data.message,
              image: '/images/error.png',
              duration: 1500
            })
            return false;
          }
        },
        fail: function (err) { },//请求失败
        complete: function (suc) {//请求完成后执行的函数

        }
      })
    })
  },
  // 验证码倒计时
  getCode: function (options) {
    var that = this;
    var currentTime = that.data.currentTime
    interval = setInterval(function () {
      currentTime--;
      that.setData({
        time: currentTime + '秒'
      })
      if (currentTime <= 0) {
        clearInterval(interval)
        that.setData({
          time: '重新发送',
          currentTime: 60,
          disabled: false
        })
      }
    }, 1000)
  },

  //点击企业认证
  identify: function(){
    wx.showToast({
      title: '即将来临，敬请期待哦~',
      icon: 'none',
    })
  },

  inputBlur: function (e) {
    var that = this;
    var id = e.currentTarget.dataset.id;
    var value = e.detail.value;
    //电话号码设置
    if (id == "phoneNo") {
      that.setData({
        phoneNo: value,
      })
      //验证码设置
    } else if (id == "idCode") {
      that.setData({
        idCode: value,
      })
      //账户名设置
    } else if (id == "username") {
      that.setData({
        username: value,
      })
      //原密码输入
    } else if (id == "passwordY"){
      that.setData({
        passwordY: value,
      })
      //密码设置
    } else if (id == "password") {
      that.setData({
        password: value,
      })
      if (value == '') {
        that.setData({
          passwordAlert: false,
        })
      } else if (value.length < 8 && value.length > 0 || value.length > 18) {
        if (value != that.data.passwordCon && that.data.passwordCon != '') {
          that.setData({
            passwordAlert: true,
            passwordconAlert: true,
          })
          wx.showToast({
            icon: 'none',
            title: '密码长度应为8~18个字符',
            mask: true,
            duration: 900,
          })
        } else {
          that.setData({
            passwordAlert: true,
            passwordconAlert: false,
          })
          wx.showToast({
            icon: 'none',
            title: '密码长度应为8~18个字符',
            mask: true,
            duration: 900,
          })
        }
      } else {
        if (value != that.data.passwordCon && that.data.passwordCon != '') {
          that.setData({
            passwordAlert: false,
            passwordconAlert: true,
          })
        } else {
          that.setData({
            passwordAlert: false,
            passwordconAlert: false,
          })
        }
      }
      //确认密码设置
    } else if (id == "passwordCon") {
      that.setData({
        passwordCon: value,
      })
      if (value != that.data.password && value != '') {
        that.setData({
          passwordconAlert: true,
        })
        wx.showToast({
          icon: 'none',
          title: '两次填写的密码不一致',
          mask: true,
          duration: 900,
        })
      } else {
        that.setData({
          passwordconAlert: false,
        })
      }
    }
  },
  // 获取旧密码
  oldPass:function(e){
    this.data.oldPass = e.detail.value
  },
  // 获取新密码
  newPass: function (e) {
    this.data.newPass = e.detail.value
  },
  // 获取再次确认密码
  comfPass: function (e) {
    this.data.comfPass = e.detail.value
  },
  submitUserpb: function () {
    var that = this;
    app.getUserInfo(function (personInfo) {
      var openid = personInfo.openId;
      var token = personInfo.token;
      var uid = personInfo.uid;
      //判断是第一次设置账户还是已有账户
      var wheInput = that.data.wheInput;
      if (wheInput == false){
        //请求个人信息
        wx.request({
          url: port.Userpb,//请求地址
          header: {//请求头
            "Content-Type": "applciation/json",
            "Authorization": "Bearer " + token
          },
          data: {
            username: that.data.username,
            mobile: that.data.phoneNo,
            openid: openid,
            code: that.data.idCode,
            password1: that.data.password,
            password2: that.data.passwordCon,
          },
          method: "post",//get为默认方法/POST
          success: function (res) {
            if (res.data.status == "error") {
              wx.showToast({
                title: res.data.message,
                image: '/images/error.png',
                duration: 900,
              })
            } else if (res.data.status == "success") {
              wx.showToast({
                title: res.data.message,
                icon: 'success',
                duration: 900,
              })
              wx.switchTab({
                url: '../../myCenter/myCenter',
              })
            }
          },
          fail: function (err) { },//请求失败
          complete: function (suc) {//请求完成后执行的函数
          }
        })
      }else if (wheInput == true){
        //修改个人密码
        wx.request({
          url: port.wechatuserpassupdate,//请求地址
          header: {//请求头
            "Content-Type": "applciation/json",
            "Authorization": "Bearer " + token
          },
          data: {
            uid: that.data.uid,
            oldpassword:that.data.oldPass,
            password1: that.data.newPass,
            password2: that.data.comfPass,
          },
          method: "post",//get为默认方法/POST
          success: function (res) {
            if (res.data.status == "error") {
              wx.showToast({
                title: res.data.message,
                image: '/images/error.png',
                duration: 900,
              })
            } else if (res.data.status == "success") {
              wx.showToast({
                title: res.data.message,
                icon: 'success',
                duration: 1500,
              })
              setTimeout(function () {
                wx.switchTab({
                  url: '../../myCenter/myCenter',
                })
              }, 1500);
              
            }
          },
          fail: function (err) { },//请求失败
          complete: function (suc) {//请求完成后执行的函数
          }
        })
      }

    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})