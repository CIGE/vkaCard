function messageCheck(avatar, username, mobile, code, codeShow, company, duty, address, ssq, imgPath) {
  // if (!avatar) {
  //   avatar = imgPath + 'new-background/logo_aboutus.png';
  // }
  // if (username != "") {
  //   var strLength = chargeString(username);
  //   if (strLength > 8) {
  //     wx.showToast({
  //       title: '姓名过长了哦',
  //       icon: 'none',
  //       mask: true,
  //       duration: 1500
  //     })
  //     return false
  //   }
  // } else 
  if (username == "") {
    wx.showToast({
      title: '姓名不能为空哦',
      icon: 'none',
      mask: true,
      duration: 1500
    })
    return false
  }
  var myreg = /^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\d{8}$/;
  if (!myreg.test(mobile)) {
    wx.showToast({
      title: '请输入正确的手机号',
      icon: 'none',
      mask: true,
      duration: 1500
    })
    return false;
  }
  // if (codeShow) {
  //   var is_code = 1;
  //   // that.setData({
  //   //   is_code:is_code
  //   // })
  //   if (!code) {
  //     wx.showToast({
  //       title: '验证码不能为空',
  //       imgs: '/images/error.png',
  //       mask: true,
  //       duration: 1500
  //     })
  //     return false
  //   }
  // }
  // if (company != "") {
  //   var strLength = chargeString(company);
  //   if (strLength > 78) {
  //     wx.showToast({
  //       title: '公司过长了哦，精简一下呗~',
  //       icon: 'none',
  //       mask: true,
  //       duration: 1500
  //     })
  //     return false
  //   }
  // } else 
  if (company == "") {
    wx.showToast({
      title: '公司不能为空哦',
      icon: 'none',
      mask: true,
      duration: 1500
    })
    return false
  }
  // if (duty != "") {
  //   var strLength = chargeString(duty);
  //   if (strLength > 12) {
  //     wx.showToast({
  //       title: '职务过长了哦，精简一下呗~',
  //       icon: 'none',
  //       mask: true,
  //       duration: 1500
  //     })
  //     return false
  //   }
  // } 
  // else
  // if (duty == "") {
  //   wx.showToast({
  //     title: '职务不能为空哦',
  //     icon: 'none',
  //     mask: true,
  //     duration: 1500
  //   })
  //   return false
  // }
  // if (ssq == "") {
  //   wx.showToast({
  //     title: '请选择一个省市区哦',
  //     icon: 'none',
  //     mask: true,
  //     duration: 1500
  //   })
  //   return false
  // }
  if (address != "") {
    var value = ssq + address
    var strLength = chargeString(value);
    if (strLength > 64) {
      wx.showToast({
        title: '详细地址过长了哦，精简一下呗~',
        icon: 'none',
        mask: true,
        duration: 1500
      })
      return false
    }
  }
  //  else {
  //   wx.showToast({
  //     title: '详细地址不能为空哦',
  //     icon: 'none',
  //     mask: true,
  //     duration: 1500
  //   })
  //   return false
  // }
}

function chargeString(value) {
  var value = value;
  var strLength = 0;
  for (var i = 0; i < value.length; i++) {
    var type = value.charCodeAt(i)
    if (type >= 19968 && type <= 40869) { //为中文字符
      strLength = strLength + 2;
    } else if (type >= 48 && type <= 57) {  //为数字字符
      strLength++;
    } else if (type >= 65 && type <= 122) {  //为英文字符
      strLength++;
    } else {  //其他字符
      strLength++;
    }
  }
  return strLength;
}

module.exports.messageCheck = messageCheck;