var app = getApp();
const port = require('../../../../config.js');
const center = require('../center.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    animate: ["noCenter", "center", "noCenter"],
    listId: 2,
    rankingList: [],//排行榜数组
    rankingListOthers: [],//三名后
    rankingListBoss: [],//前三
    imgPath: port.imgPath, //照片路径
    rankingListBoss1: false,
    rankingListBoss2: false,
    rankingListBoss3: false,
    choiceShow1: false,
    choiceShow2: true,
    choiceShow3: false,
    lenZero: 0,
    centerShuttle: center.centerShuttle,   //跳转分包shuttle统一路径
    centerMessage: center.centerMessage,  //跳转分包message统一路径
    centerIndex: center.centerIndex,   //跳转主包统一路径

    indicatorDots: false,
    autoplay: false,
    interval: 1500,
    duration: 1000,
    circular:false,
    current:1,
    cardNum:1,
    nobody: false,   //无人上榜时展示
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.achieveF()
  },
  // 获取名人榜信息
  achieveF:function(){
    wx.hideShareMenu();
    var that = this;
    app.getUserInfo(function (personInfo) {
      var token = personInfo.token;
      var uidF = personInfo.uid;
      var openid = personInfo.openId;
      wx.request({
        url: port.getcelebrity,//请求地址
        header: {//请求头
          "Content-Type": "applciation/json",
          "Authorization": "Bearer " + token
        },
        method: "post",//get为默认方法/POST
        success: function (res) {
          if (res.data.status == 'success') {
            if (that.data.listId == 2) {
              var famousArr = new Array();
              var length = res.data.data.length;
              for (var j = 0; j <length; j++) {
                var famousObject = new Object();
                famousObject.ida = res.data.data[j].id;
                famousObject.id= res.data.data[j].j;
                famousObject.show_pic = res.data.data[j].show_pic;
                famousObject.username = res.data.data[j].username;
                famousObject.duty = res.data.data[j].duty;
                famousObject.company = res.data.data[j].company;
                famousObject.industry = res.data.data[j].industry;
                // if (that.data.cardNum==j) {
                //   famousObject.animateY = "center";
                // } else {
                //   famousObject.animateY = "noCenter";
                // }
                famousArr.push(famousObject);
              }
              that.setData({
                famousArr: famousArr,
                length: length
              })
            }
          }

        },
        fail: function (err) {
          wx.hideLoading()
        },//请求失败
        complete: function (suc) {//请求完成后执行的函数
        }
      })
    })
  },
  //预览
  goView: function (e) {
    wx.navigateTo({
      url: '/pages/index/preview?cid=' + e.currentTarget.id, //单击获取对应id
    })
  },
  goViewF: function (e) {
    wx.navigateTo({
      url: '/pages/index/preview?cid=' + e.currentTarget.id, //单击获取对应id
    })
  },
  /* 选择总周日榜界面 */
  choiceList: function (e) {
    var listId = e.target.dataset.listid;
    if (listId == 1) {
      var choiceShow1 = true;
      var choiceShow2 = false;
      var choiceShow3 = false;
    }
    if (listId == 2) {
      var choiceShow1 = false;
      var choiceShow2 = true;
      var choiceShow3 = false;
      
    }
    if (listId == 3) {
      var choiceShow1 = false;
      var choiceShow2 = false;
      var choiceShow3 = true;
    }
    this.setData({
      listId: listId,
      choiceShow1: choiceShow1,
      choiceShow2: choiceShow2,
      choiceShow3: choiceShow3
    })
    
    var that = this;
    wx.showLoading({
      title: '超努力中...',
    })
    that.setData({
      nobody: false
    })
    app.getUserInfo(function (personInfo) {
      var token = personInfo.token;
      var uid = personInfo.uid;
      var openid = personInfo.openId;
      wx.request({
        url: port.invitinglist,//请求地址
        header: {//请求头
          "Content-Type": "applciation/json",
          "Authorization": "Bearer " + token
        },
        data: { 'openid': openid },
        method: "post",//get为默认方法/POST
        success: function (res) {
          // 周榜
          if (that.data.listId == 1) {
            if (res.data.data.week.length == 1) {
              var rankingListBoss1 = true;
              var rankingListBoss2 = false;
              var rankingListBoss3 = false;
              that.setData({
                rankingListBoss1: rankingListBoss1,
                rankingListBoss2: rankingListBoss2,
                rankingListBoss3: rankingListBoss3
              })
            }
            if (res.data.data.week.length == 2) {
              var rankingListBoss1 = true;
              var rankingListBoss2 = true;
              var rankingListBoss3 = false;
              that.setData({
                rankingListBoss1: rankingListBoss1,
                rankingListBoss2: rankingListBoss2,
                rankingListBoss3: rankingListBoss3
              })
            }
            if (res.data.data.week.length >= 3) {
              var rankingListBoss1 = true;
              var rankingListBoss2 = true;
              var rankingListBoss3 = true;
              that.setData({
                rankingListBoss1: rankingListBoss1,
                rankingListBoss2: rankingListBoss2,
                rankingListBoss3: rankingListBoss3
              })
            }

            var rankingList = new Array();
            for (var j = 0; j < res.data.data.week.length; j++) {
              var rankingListObject = new Object();
              rankingListObject.id = res.data.data.week[j].id;
              rankingListObject.num = j + 1;
              rankingListObject.avatar = res.data.data.week[j].avatar;
              rankingListObject.username = res.data.data.week[j].username;
              rankingListObject.inviting_count = res.data.data.week[j].inviting_count;
              rankingListObject.duty = res.data.data.week[j].duty;
              rankingListObject.company = res.data.data.week[j].company;
              rankingList.push(rankingListObject);
              //排名前三
              var rankingListBoss = new Array();
              rankingListBoss = rankingList.slice(0, 4);
              //排名前三结束
              // 排名三名以后
              var rankingListOthers = new Array();
              rankingListOthers = rankingList.slice(3);
              // 排名三名以后结束
            }
            if (res.data.data.week.length == 0) {
              var rankingListBoss1 = false;
              var rankingListBoss2 = false;
              var rankingListBoss3 = false;
              that.setData({
                rankingListBoss1: rankingListBoss1,
                rankingListBoss2: rankingListBoss2,
                rankingListBoss3: rankingListBoss3,
                nobody: true,   //无人上榜时展示
              })
              var rankingList = new Array();
              for (var j = 0; j < 3; j++) {
                var rankingListObject = new Object();
                rankingListObject.id = 0;
                rankingListObject.num = 0;
                rankingListObject.avatar = 0;
                rankingListObject.username = 0;
                rankingListObject.inviting_count = 0;
                rankingListObject.duty = 0;
                rankingListObject.company = 0;
                rankingList.push(rankingListObject);
                //排名前三
                var rankingListBoss = new Array();
                rankingListBoss = rankingList.slice(0, 4);
                //排名前三结束
                // 排名三名以后
                var rankingListOthers = new Array();
                rankingListOthers = rankingList.slice(3);
                // 排名三名以后结束
              }
            }else{
              that.setData({
                nobody: false,   //有人上榜时
              })
            }
            that.setData({
              rankingList: rankingList,
              rankingListBoss: rankingListBoss,
              rankingListOthers: rankingListOthers,
            })
            wx.hideLoading();
          }
          // 周榜结束
          // 名人榜
          if (that.data.listId == 2) {
            that.achieveF()
            wx.hideLoading();
          }
          // 名人榜结束
          // 日榜
          if (that.data.listId == 3) {
            if (res.data.data.day.length == 1) {
              var rankingListBoss1 = true;
              var rankingListBoss2 = false;
              var rankingListBoss3 = false;
              that.setData({
                rankingListBoss1: rankingListBoss1,
                rankingListBoss2: rankingListBoss2,
                rankingListBoss3: rankingListBoss3
              })
              var rankingList = new Array();
              for (var j = 0; j < res.data.data.day.length; j++) {
                var rankingListObject = new Object();
                rankingListObject.id = res.data.data.day[j].id;
                rankingListObject.num = j + 1;
                rankingListObject.avatar = res.data.data.day[j].avatar;
                rankingListObject.username = res.data.data.day[j].username;
                rankingListObject.inviting_count = res.data.data.day[j].inviting_count;
                rankingListObject.duty = res.data.data.day[j].duty;
                rankingListObject.company = res.data.data.day[j].company;
                rankingList.push(rankingListObject);
                //排名前三
                var rankingListBoss = new Array();
                rankingListBoss = rankingList.slice(0, 4);
                //排名前三结束
                // 排名三名以后
                var rankingListOthers = new Array();
                rankingListOthers = rankingList.slice(3);
                // 排名三名以后结束
              }
              that.setData({
                rankingList: rankingList,
                rankingListBoss: rankingListBoss,
                rankingListOthers: rankingListOthers,
              })
            }
            if (res.data.data.day.length == 2) {
              var rankingListBoss1 = true;
              var rankingListBoss2 = true;
              var rankingListBoss3 = false;
              that.setData({
                rankingListBoss1: rankingListBoss1,
                rankingListBoss2: rankingListBoss2,
                rankingListBoss3: rankingListBoss3
              })
            }
            if (res.data.data.day.length >= 3) {
              var rankingListBoss1 = true;
              var rankingListBoss2 = true;
              var rankingListBoss3 = true;
              that.setData({
                rankingListBoss1: rankingListBoss1,
                rankingListBoss2: rankingListBoss2,
                rankingListBoss3: rankingListBoss3
              })
              var rankingList = new Array();
              for (var j = 0; j < res.data.data.day.length; j++) {
                var rankingListObject = new Object();
                rankingListObject.id = res.data.data.day[j].id;
                rankingListObject.num = j + 1;
                rankingListObject.avatar = res.data.data.day[j].avatar;
                rankingListObject.username = res.data.data.day[j].username;
                rankingListObject.inviting_count = res.data.data.day[j].inviting_count;
                rankingListObject.duty = res.data.data.day[j].duty;
                rankingListObject.company = res.data.data.day[j].company;
                rankingList.push(rankingListObject);
                //排名前三
                var rankingListBoss = new Array();
                rankingListBoss = rankingList.slice(0, 4);
                //排名前三结束
                // 排名三名以后
                var rankingListOthers = new Array();
                rankingListOthers = rankingList.slice(3);
                // 排名三名以后结束
              }
              that.setData({
                rankingList: rankingList,
                rankingListBoss: rankingListBoss,
                rankingListOthers: rankingListOthers,
              })
            }
            var rankingList = new Array();
            for (var j = 0; j < res.data.data.day.length; j++) {
              var rankingListObject = new Object();
              rankingListObject.id = res.data.data.day[j].id;
              rankingListObject.num = j + 1;
              rankingListObject.avatar = res.data.data.day[j].avatar;
              rankingListObject.username = res.data.data.day[j].username;
              rankingListObject.inviting_count = res.data.data.day[j].inviting_count;
              rankingListObject.duty = res.data.data.day[j].duty;
              rankingListObject.company = res.data.data.day[j].company;
              rankingList.push(rankingListObject);
              //排名前三
              var rankingListBoss = new Array();
              rankingListBoss = rankingList.slice(0, 4);
              //排名前三结束
              // 排名三名以后
              var rankingListOthers = new Array();
              rankingListOthers = rankingList.slice(3);
              // 排名三名以后结束
            }

            if (res.data.data.day.length == 0) {
              var rankingListBoss1 = false;
              var rankingListBoss2 = false;
              var rankingListBoss3 = false;
              that.setData({
                rankingListBoss1: rankingListBoss1,
                rankingListBoss2: rankingListBoss2,
                rankingListBoss3: rankingListBoss3,
                nobody: true,   //无人上榜时展示
              })
              var rankingList = new Array();
              for (var j = 0; j < 3; j++) {
                var rankingListObject = new Object();
                rankingListObject.id = 0;
                rankingListObject.num = 0;
                rankingListObject.avatar = 0;
                rankingListObject.username = 0;
                rankingListObject.inviting_count = 0;
                rankingListObject.duty = 0;
                rankingListObject.company = 0;
                rankingList.push(rankingListObject);
                //排名前三
                var rankingListBoss = new Array();
                rankingListBoss = rankingList.slice(0, 4);
                //排名前三结束
                // 排名三名以后
                var rankingListOthers = new Array();
                rankingListOthers = rankingList.slice(3);
                // 排名三名以后结束
              }
            } else {
              that.setData({
                nobody: false,   //有人上榜时
              })
            }
            that.setData({
              rankingList: rankingList,
              rankingListBoss: rankingListBoss,
              rankingListOthers: rankingListOthers,
            })
            wx.hideLoading()
          }
          // 日榜结束
        },
        fail: function (err) {
          wx.hideLoading()
        },//请求失败
        complete: function (suc) {//请求完成后执行的函数
        }
      })
    })
  },
  /* 选择总周日榜界面结束 */
  //交换申请
  exchange: function (e) {
    var that = this;
    //显示申请消息输入框
    this.setData({
      showModal: true
    })
    //设置被请求者cardid
    that.setData({
      plus_cardid: e.currentTarget.id,
      card: that.data.rankingList[e.currentTarget.dataset.id],
      cardId: e.currentTarget.dataset.id
    })
  },
  //名人交换申请
  exchangeF: function (e) {
    var that = this;
    //显示申请消息输入框
    this.setData({
      showModalF: true
    })
    // 设置被请求者cardid
    that.setData({
      plus_cardid: e.currentTarget.id,
      card: that.data.famousArr[e.currentTarget.dataset.id],
      cardId: e.currentTarget.dataset.id
    })
  },
  move:function(e){
    var that=this;
    var cardNum = e.detail.current;
    var animate = new Array();
    for (var i = 0; i < that.data.length;i++){
      if (cardNum==i){
        animate[cardNum] = "center";    
      }else{
        animate[i] = "noCenter";    
      }
    }
    this.setData({
      cardNum: cardNum,
      animate: animate
    })
  },
  formsubmit: function (e) {
    var that = this;
    // 设置表单信息
    var addMsg = e.detail.value.addMsg;
    if (addMsg == '' || addMsg == null || addMsg.match(/^\s*$/)) {
      addMsg = "请求交换名片";
    }
    that.setData({
      formid: e.detail.formId,
      apply_text: addMsg
    })
    
    if (that.data.listId==2){
      //发送申请请求
      app.getUserInfo(function (personInfo) {
        var token = personInfo.token;
        var uid = personInfo.uid;
        //发送请求交换信息
        wx.request({
          url: port.CardApply,
          method: 'POST',
          data: {
            openid: personInfo.openId,
            cardid: that.data.cardid,
            plus_cardid: that.data.plus_cardid,
            formid: that.data.formid,
            apply_text: that.data.apply_text,
          },
          header: {
            //data为'pageSize=1&pageNum=10'类时，设置参数内容类型为x-www-form-urlencoded
            'content-type': 'application/json',
            "Authorization": "Bearer " + token
          },
          success: function (res) {
            that.setData({
              showModalF: false
            });
            wx.showToast({
              title: res.data.message,
            })
          }
        })
      })
    }else{
      //发送申请请求
      app.getUserInfo(function (personInfo) {
        var token = personInfo.token;
        var uid = personInfo.uid;

        //发送请求交换信息
        wx.request({
          url: port.CardApply,
          method: 'POST',
          data: {
            openid: personInfo.openId,
            cardid: that.data.rankingList[that.data.cardId].uid,
            plus_cardid: that.data.rankingList[that.data.cardId].id,
            formid: that.data.formid,
            apply_text: that.data.apply_text,
          },
          header: {
            //data为'pageSize=1&pageNum=10'类时，设置参数内容类型为x-www-form-urlencoded
            'content-type': 'application/json',
            "Authorization": "Bearer " + token
          },
          success: function (res) {
            that.setData({
              showModal: false
            });
            wx.showToast({
              title: res.data.message,
            })
          }
        })
      })
    }
   
    
  },
  //申请界面
  hideModal: function () {
    this.setData({
      showModal: false
    });
  },
  hideModalF: function () {
    this.setData({
      showModalF: false
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})