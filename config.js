/**
 * 小程序配置文件
 */

var host = "card.zjmz888.com";
var imgPath = "https://mzcard.oss-cn-hangzhou.aliyuncs.com/";
var code = "https://card.zjmz888.com/code/";
var xtimg = "https://card.zjmz888.com/images/xcx/";
var config = {

  // 下面的地址配合云端 Server 工作
  host,
  code,
  xtimg,
  //图片链接地址
  imgPath,
  //获取微信手机
  wechatMobile: `https://${host}/api/wechatMobile`,
  //用户登录获取解密用户信息，openid，接口所需token
  UserEncryptedData: `https://${host}/api/wechatlogin`,
  //我的名片
  CardMy: `https://${host}/api/v1/CardMy`,
  //相册加载
  CardMyImg: `https://${host}/api/v1/CardMyImg`,
  //名片添加
  CardAdd: `https://${host}/api/v1/CardAdd`,
  //名片修改
  CardUpdate: `https://${host}/api/v1/CardUpdate`,
  //公开库
  CardAll: `https://${host}/api/v1/CardAll`,
  //查看名片
  CardLook: `https://${host}/api/v1/CardLook`,
  //收藏名片
  CardCollect: `https://${host}/api/v1/CardCollect`,
  //收藏名片
  CardBrowseRecord: `https://${host}/api/v1/CardBrowseRecord`,
  //行业
  CardIndustry: `https://${host}/api/v1/CardIndustry`,
  //相册信息
  CardAlbumList: `https://${host}/api/v1/CardAlbumList`,
  //删除相册
  CardAlbumDel: `https://${host}/api/v1/AlbumDel`,
  //创建相册
  CreateAlbum: `https://${host}/api/v1/Createalbum`,
  //个人中心
  Personal: `https://${host}/api/v1/Personal`,
  //查看相册
  CardAlbuminfo: `https://${host}/api/v1/CardAlbuminfo`,
  //我的下级
  CardDown: `https://${host}/api/v1/CardDown`,
  //签到
  Cardsign: `https://${host}/api/v1/Cardsign`,
  //上传图片
  upload: `https://${host}/api/upload`,
  //上传视频
  uploadVideo: `https://${host}/api/uploadVideo`,
  //视频删除
  DelVideo: `https://${host}/api/v1/DelVideo`,
  //文件删除
  FileDel: `https://${host}/api/FileDel`,
  //小程序码
  Code: `https://${host}/api/v1/Code`,
  //配置
  Config: `https://${host}/api/v1/Config`,
  //手机验证码发送
  CodeSMS: `https://${host}/api/v1/CodeSMS`,
  Config: `https://${host}/api/v1/Config`,
  //交换申请
  CardApply: `https://${host}/api/cardapply`,
  //审核申请
  CardSwap: `https://${host}/api/cardswap`,
  //交换过关系
  FriendsList: `https://${host}/api/friendslist`,
  //绑定账号
  Userpb: `https://${host}/api/v1/wechatuserpb`,
  //删除搜索记录
  SearchDel: `https://${host}/api/v1/usersearchdel`,
  // 积分详情
  PointsDetail: `https://${host}/api/v1/useraccount`,
  //交换消息列表
  applylist: `https://${host}/api/applylist`,
  //排行榜列表
  invitinglist: `https://${host}/api/v1/invitinglist`,
  //名人榜
  getcelebrity: `https://${host}/api/v1/getcelebrity`,
  //投诉建议
  messageboard: `https://${host}/api/v1/messageboard`,
  //判断是否绑定账号
  checkuserinfo: `https://${host}/api/v1/getuserinfo`,
  //ORC授权签名
  CardOCRAuth: `https://${host}/api/v1/CardOCRAuth`,
  //咖片夹添加纸质名片
  papercreate: `https://${host}/api/v1/papercreate`,
  //咖片夹修改纸质名片
  paperupdate: `https://${host}/api/v1/paperupdate`,
  //咖片夹查看纸质名片
  papershow: `https://${host}/api/v1/papershow`,
  //咖片夹查看纸质名片
  paperdelete: `https://${host}/api/v1/paperdelete`,
  //安全中心密码修改
  wechatuserpassupdate: `https://${host}/api/v1/wechatuserpassupdate`,
  //广播消息
  broadcastMessage: `https://${host}/api/v1/information`,
  //判断该用户是否是会员
  judgeVip: `https://${host}/api/v1/getvip`,
  //会员特权
  vipPrivilege: `https://${host}/api/v1/jfj`,
  //微信支付参数请求
  payFor: `https://${host}/api/v1/vippay`,
  // 会员支付金额
  config: `https://${host}/api/v1/config`,
  // 获取会员领取积分状态
  getviplog: `https://${host}/api/v1/getviplog`,
  // 领取积分
  makeviplog: `https://${host}/api/v1/makeviplog`,
}
module.exports = config
