const port = require('../config.js');
function vipPrivilege(token, uid, cid, action, cb) {
  var that = this;
  var token = token;
  var uid = uid;
  var action = action;
  if(cid != ''){
    cid = cid;
  }else{
    cid = 0
  }
  wx.request({
    url: port.vipPrivilege,//请求地址
    header: {//请求头
      "Content-Type": "applciation/json",
      "Authorization": "Bearer " + token
    },
    data: { uid: uid, action: action, cid: cid },//action: look 查看名片  scanning：添加名片
    method: "post",//get为默认方法/POST
    success: function (res) {
      if (res.data.data.return_code == 'success') {
        return typeof cb == "function" && cb(res.data.data.return_code)
      } else {
        if (res.data.data.return_code == 'vip-deficiency') {   //VIP权限上限已达，但积分不足
          return typeof cb == "function" && cb(res.data.data.return_code)
        } else if (res.data.data.return_code = 'deficiency') {    //非会员，且积分不足
          return typeof cb == "function" && cb(res.data.data.return_code)
        } else {
          return typeof cb == "function" && cb(res.data.data.return_code)
        }
      }
    },
    fail: function (err) { },//请求失败
    complete: function (suc) {//请求完成后执行的函数
    }
  })

}

module.exports.vipPrivilege = vipPrivilege;