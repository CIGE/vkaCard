var app = getApp();
const port = require('../../../../config.js');
const center = require('../center.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    txShow: true,
    imgPath: port.imgPath,
    noBody: true,
    centerShuttle: center.centerShuttle,   //跳转分包shuttle统一路径
    centerMessage: center.centerMessage,  //跳转分包message统一路径
    centerIndex: center.centerIndex,   //跳转主包统一路径
    is_vip: false,   //会员logo
    vlogo: false,   //合伙人logo
    pages: 1,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.hideShareMenu();
    var that = this;
    app.getUserInfo(function (personInfo) {
      var token = personInfo.token;
      var uid = personInfo.uid;
      wx.request({
        url: port.CardDown + '?pages=' + that.data.pages,//请求地址
        header: {//请求头
          "Content-Type": "applciation/json",
          "Authorization": "Bearer " + token
        },
        data: { 'uid': uid, 'status': 2 },
        method: "post",//get为默认方法/POST
        success: function (res) {
          if (res.data.status == 'success') {
            if (res.data.data != '') {
              that.setData({
                list: res.data.data.data,
                txShow: false,
                noBody: false
              })
            }
          }
          else {
            that.setData({
              txShow: true
            })
          }
        },
        fail: function (err) { },//请求失败
        complete: function (suc) {//请求完成后执行的函数

        }
      })
    })
  },

  //查看名片
  preview: function(option){
    var that = this;
    var cid = option.currentTarget.dataset.cid;
    wx.navigateTo({
      url: '/pages/index/preview?cid=' + cid + '&my=0',
    })
  },

  //交换申请
  exchange: function (e) {
    console.log(e)
    var that = this;
    //显示申请消息输入框
    this.setData({
      showModal: true
    })
    //设置被请求者cardid
    that.setData({
      plus_cardid: e.currentTarget.id,
      card: that.data.list[e.currentTarget.dataset.id],
      cardId: e.currentTarget.dataset.id
    })
  },
  //判断长度
  chargeString: function (value) {
    var value = value;
    var strLength = 0;
    for (var i = 0; i < value.length; i++) {
      var type = value.charCodeAt(i)
      if (type >= 19968 && type <= 40869) { //为中文字符
        strLength = strLength + 2;
      } else if (type >= 48 && type <= 57) {  //为数字字符
        strLength++;
      } else if (type >= 65 && type <= 122) {  //为英文字符
        strLength++;
      } else {  //其他字符
        strLength++;
      }
    }
    return strLength;
  },
  formsubmit: function (e) {
    var that = this
    // 设置表单信息
    var liuyan = that.chargeString(e.detail.value.addMsg);
    if (liuyan > 68) {
      wx.showToast({
        title: '留言不可超过30个字哦~',
        icon: 'none',
        mask: true,
        duration: 1500
      })
      return
    }
    that.setData({
      formid: e.detail.formId,
      apply_text: e.detail.value.addMsg
    })
    //发送申请请求
    app.getUserInfo(function (personInfo) {
      var token = personInfo.token;
      var uid = personInfo.uid;
      //发送请求交换信息
      wx.request({
        url: port.CardApply,
        method: 'POST',
        data: {
          openid: personInfo.openId,
          cardid: that.data.cardid,
          plus_cardid: that.data.plus_cardid,
          formid: that.data.formid,
          apply_text: that.data.apply_text,
        },
        header: {
          //data为'pageSize=1&pageNum=10'类时，设置参数内容类型为x-www-form-urlencoded
          'content-type': 'application/json',
          "Authorization": "Bearer " + token
        },
        success: function (res) {
          that.setData({
            showModal: false
          });
          wx.showToast({
            title: res.data.message,
          })
        }
      })
    })
  },
  //申请界面
  showDialogBtn: function () {
    this.setData({
      showModal: true
    })
  },
  hideModal: function () {
    this.setData({
      showModal: false
    });
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.onLoad()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    var pages = that.data.pages;
    pages++;
    console.log(pages)
    that.setData({
      pages:pages
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})