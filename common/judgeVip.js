const port = require('../config.js');
function judgeVip(token, uid, cb) {
  wx.request({
    url: port.judgeVip,//请求地址
    header: {//请求头
      "Content-Type": "applciation/json",
      "Authorization": "Bearer " + token
    },
    data: { uid: uid },
    method: "post",//get为默认方法/POST
    success: function (res) {
      if (res.data.status == 'success') {
        return typeof cb == "function" && cb(res.data.data)
      } else {
        return typeof cb == "function" && cb(res.data.data)
      }
    },
    fail: function (err) { },//请求失败
    complete: function (suc) {//请求完成后执行的函数
    }
  })
}

module.exports.judgeVip = judgeVip;