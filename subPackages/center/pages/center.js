/**
 * 分包center配置文件
 */

var centerShuttle = '../../../shuttle/pages';   //跳转分包shuttle统一路径
var centerMessage = '../../../message/pages';  //跳转分包message统一路径
var centerIndex = '../../../../pages/index';   //跳转主包统一路径

var center = {
  centerShuttle,
  centerMessage,
  centerIndex,
}
module.exports = center