//app.js

const UserEncryptedData = require('./config').UserEncryptedData;
const vipPrivilege = require('common/vipPrivilege.js')

App({

  globalData: {
    UrlToken: "",//全局使用接口Token
    personInfo: "",//全局用户信息
    isConnected: true,
  },
  onLaunch: function () {
    var that = this;
  },
  NetworkState: function () {
    wx.getNetworkType({
      success: function (res) {
        // 返回网络类型, 有效值：
        // wifi/2g/3g/4g/unknown(Android下不常见的网络类型)/none(无网络)
        var networkType = res.networkType
        if (networkType == "none") {
          wx.hideLoading()
          wx.showModal({
            showCancel: false,
            content: '网络连接出错！',
            confirmText: '我知道了',
          })
        }
      }
    })
  },
  // 全局获取用户信息函数
  getUserInfo: function (cb) {
    var that = this;
    var UserCache = wx.getStorageSync('WxLoginUserInfo');
    // var UserCache = '';
    if (UserCache) {
      typeof cb == "function" && cb(UserCache)
    } else {
      //调用登录接口
      wx.login({
        success: function (res) {
          if (res.code) {
            // 用户登录
            //发起网络请求  
            wx.request({
              //后台请求用户session_key,解密用户信息接口
              url: UserEncryptedData,
              data: {
                code: res.code,
              },
              method: 'POST',
              header: {
                'content-type': 'application/json'
              },
              success: function (res) {
                // this.globalData.userInfo = JSON.parse(res.data);

                //广播本地缓存
                that.broadcast();

                //智能引导本地缓存
                that.storage();

                //人脉圈积分扣除弹窗缓存
                that.showDeduct();

                //咖片夹积分扣除弹窗缓存
                that.holderDeduct();

                var data = res.data;
                that.globalData.personInfo = data;
                that.globalData.UrlToken = data.token;

                wx.removeStorageSync('WxLoginUserInfo')
                wx.setStorageSync('WxLoginUserInfo', data)
                typeof cb == "function" && cb(that.globalData.personInfo)
              }
            })
          }
        }
      })
    }
  },

  //咖片夹积分扣除弹窗缓存
  holderDeduct: function(){
    wx.getStorage({
      key: 'holderdeduct',
      success: function (res) {
        if (res.data == 0) {
          wx.setStorage({
            key: 'holderdeduct',
            data: 0,
          })
        }
      },
      fail: function (res) {
        wx.setStorage({
          key: 'holderdeduct',
          data: 0,
        })
      }
    })
  },

  //人脉圈积分扣除弹窗缓存
  showDeduct: function(){
    wx.getStorage({
      key: 'showdeduct',
      success: function (res) {
        if (res.data == 0) {
          wx.setStorage({
            key: 'showdeduct',
            data: 0,
          })
        }
      },
      fail: function (res) {
        wx.setStorage({
          key: 'showdeduct',
          data: 0,
        })
      }
    })
  },

  //广播本地缓存
  broadcast: function(){
    wx.getStorage({
      key: 'broadcast',
      success: function (res) {
        if (res.data == 0) {
          wx.setStorage({
            key: 'broadcast',
            data: 0,
          })
        }
      },
      fail: function (res) {
        wx.setStorage({
          key: 'broadcast',
          data: 0,
        })
      }
    })
  },

  //智能引导本地缓存
  storage: function () {
    wx.getStorage({
      key: 'cardCheck',
      success: function (res) {
        if (res.data == 0) {
          wx.setStorage({
            key: 'cardCheck',
            data: 0,
          })
        }
      },
      fail: function (res) {
        wx.setStorage({
          key: 'cardCheck',
          data: 0,
        })
      }
    })
    wx.getStorage({
      key: 'centerCheck',
      success: function (res) {
        if (res.data == 0) {
          wx.setStorage({
            key: 'centerCheck',
            data: 0,
          })
        }
      },
      fail: function (res) {
        wx.setStorage({
          key: 'centerCheck',
          data: 0,
        })
      }
    })
    wx.getStorage({
      key: 'libraryCheck',
      success: function (res) {
        if (res.data == 0) {
          wx.setStorage({
            key: 'libraryCheck',
            data: 1,
          })
        }
      },
      fail: function (res) {
        wx.setStorage({
          key: 'libraryCheck',
          data: 1,
        })
      }
    })
    wx.getStorage({
      key: 'holderCheck',
      success: function (res) {
        if (res.data == 0) {
          wx.setStorage({
            key: 'holderCheck',
            data: 0,
          })
        }
      },
      fail: function (res) {
        wx.setStorage({
          key: 'holderCheck',
          data: 0,
        })
      }
    })
  },

  uploadimg: function (data) {
    var that = this,
      i = data.i ? data.i : 0,//当前上传的哪张图片
      success = data.success ? data.success : 0,//上传成功的个数
      fail = data.fail ? data.fail : 0;//上传失败的个数
    that.getUserInfo(function (personInfo) {
      var uid = personInfo.uid;
      var timestamp = Date.parse(new Date()) + i + ".jpg";
      wx.uploadFile({
        url: data.url,
        filePath: data.path[i],
        name: 'file',//这里根据自己的实际情况改
        formData: {
          name: timestamp,
          uid: uid
        },
        success: (resp) => {
          success++;//图片上传成功，图片上传成功的变量+1
          console.log(resp)
          console.log(i);
          //这里可能有BUG，失败也会执行这里,所以这里应该是后台返回过来的状态码为成功时，这里的success才+1
        },
        fail: (res) => {
          fail++;//图片上传失败，图片上传失败的变量+1
          console.log('fail:' + i + "fail:" + fail);
        },
        complete: () => {
          console.log(i);
          i++;//这个图片执行完上传后，开始上传下一张
          if (i == data.path.length) {   //当图片传完时，停止调用          
            console.log('执行完毕');
            console.log('成功：' + success + " 失败：" + fail);
          } else {//若图片还没有传完，则继续调用函数
            console.log(i);
            data.i = i;
            data.success = success;
            data.fail = fail;
            that.uploadimg(data);
          }
        }
      });
    })
  },

})