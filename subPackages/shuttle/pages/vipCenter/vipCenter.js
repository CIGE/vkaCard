// pages/vipCenter/vipCenter.js
var app = getApp();
const port = require('../../../../config.js');
const payFor = require('../../../../common/payFor.js');
const shuttle = require('../shuttle.js');
const judgeVip = require('../../../../common/judgeVip.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgPath: port.imgPath,
    id: 3,
    clickIcon: 1,
    selected: true,
    shuttleMessage: shuttle.shuttleMessage, //跳转分包message统一路径
    shuttleMyCenter: shuttle.shuttleMyCenter, //跳转分包myCenter统一路径
    shuttleIndex: shuttle.shuttleIndex, //跳转主包统一路径

    // 滑动
    lastX: 0,
    lastY: 0,
    currentGesture: 0,
    expiry_time: '', //会员过期日
    is_task: false  //是否可领取100积分，false不可领取，true可领取
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    // 用户信息
    wx.hideShareMenu();
    app.getUserInfo(function(personInfo) {
      var token = personInfo.token;
      var uid = personInfo.uid;
      //请求个人信息
      wx.request({
        url: port.Personal, //请求地址
        header: { //请求头
          "Content-Type": "applciation/json",
          "Authorization": "Bearer " + token
        },
        data: {
          uid: uid
        },
        method: "post", //get为默认方法/POST
        success: function(res) {
          if (res.data.status == 'success') {
            if (res.data.data1 != null && res.data.data1 != '') {
              if (res.data.data1.duty == null) {
                res.data.data1.duty = "";
              }
              that.setData({
                username: res.data.data1.username,
                avatar: res.data.data1.avatar,
                duty: res.data.data1.duty,
                company: res.data.data1.company,
                cid: res.data.data1.id,

              })
            } else {
              that.setData({
                username: res.data.data2.nickname,
                avatar: res.data.data2.avatar,
              })
            }
            if (res.data.data3 != null && res.data.data3 != '') {
              var date = new Date();
              var year = date.getFullYear()
              var month = date.getMonth() + 1
              if (month < 10) {
                month = '0' + month;
              };
              var day = date.getDate();
              if (day < 10) {
                day = '0' + day;
              };
              var time = year + '' + month + '' + day;
              if (time == res.data.data3.time) {
                that.setData({
                  signShow: false
                })
              }
              if (time - res.data.data3.time < 2) {
                if (res.data.data3.continuous >= 7) {
                  that.setData({
                    continuous: res.data.data3.continuous,
                    left: "84",
                    margin: "98",
                    one: false,
                    two: true
                  })
                } else {
                  that.setData({
                    continuous: res.data.data3.continuous,
                    left: res.data.data3.continuous * 12,
                    margin: res.data.data3.continuous * 14,
                    one: true,
                    two: false
                  })
                }
              }
            }
            that.setData({
              integral: res.data.data2.integral,
            })
          } else {
            wx.showToast({
              title: res.data.message,
              icon: 'none',
              mask: true,
              duration: 1500
            })
          }
        },
        fail: function(err) {}, //请求失败
        complete: function(suc) { //请求完成后执行的函数
        }
      })
      // 会员付款金额
      wx.request({
        url: port.config, //请求地址
        header: { //请求头
          "Content-Type": "applciation/json",
          "Authorization": "Bearer " + token
        },
        method: "post", //get为默认方法/POST
        success: function(res) {
          that.setData({
            monthM: res.data.data.vk_vip.month,
            seasonM: res.data.data.vk_vip.season,
            yearM: res.data.data.vk_vip.year
          })
        },
        fail: function(err) {
          wx.hideLoading()
        }, //请求失败
        complete: function(suc) { //请求完成后执行的函数
        }
      })
      //判断是否已绑定账号
      wx.request({
        url: port.checkuserinfo, //请求地址
        header: { //请求头
          "Content-Type": "applciation/json",
          "Authorization": "Bearer " + token
        },
        data: {
          user: uid
        },
        method: "post", //get为默认方法/POST
        success: function(res) {
          //is_pass 未绑定：0；绑定：1
          if (res.data.status == 'success') {
            var is_pass = res.data.data.is_pass;
            if (is_pass == 1) {
              that.setData({
                securityValue: '修改密码',
                centerCheck: 1,
              })
              wx.setStorage({
                key: 'centerCheck',
                data: 1,
              })
            } else if (is_pass == 0) {
              //判断是否首次进入
              var cardCheck = wx.getStorage({
                key: 'centerCheck',
                success: function(res) {
                  if (res.data == 0) {
                    that.setData({
                      centerCheck: 0,
                      securityIndex: 99
                    })
                  } else if (res.data == 1) {
                    that.setData({
                      centerCheck: 1,
                    })
                  }
                },
              })
            }
          } else {
            wx.showToast({
              title: res.data.message,
              icon: 'none',
              mask: true,
              duration: 1500
            })
          }
        },
        fail: function(err) {}, //请求失败
        complete: function(suc) { //请求完成后执行的函数
        }
      })
      //判断是否可领取积分
      wx.request({
        url: port.getviplog, //请求地址
        header: { //请求头
          "Content-Type": "applciation/json",
          "Authorization": "Bearer " + token
        },
        data: {
          uid: uid,
          action: 'take_jf'
        },
        method: "post", //get为默认方法/POST
        success: function (res) {
          if (res.data.status == 'success') {
            if (res.data.data.is_task == 0){
              that.setData({
                is_task: false
              })
            } else if(res.data.data.is_task == 1){
              that.setData({
                is_task: true
              })
            }
          } else {
          }
        },
        fail: function (err) { }, //请求失败
        complete: function (suc) { //请求完成后执行的函数
        }
      })
      //判断会员
      judgeVip.judgeVip(token, uid, function(result) {
        if (result) {
          var expiry_time = result.expiry_time;
          expiry_time = expiry_time.substr(0, 10)
          that.setData({
            is_vip: true,
            expiry_time: expiry_time
          })
        } else {
          that.setData({
            is_vip: false
          })
        }
      })
    })

  },
  //每月领取积分
  receiveJf: function(e){
    var that = this;
    var status = e.target.dataset.status;
    if(status == "no"){
      wx.showToast({
        title: '您还无法领取哦~',
        icon: 'none'
      })
    }else if(status == "yes"){
      app.getUserInfo(function (personInfo) {
        var token = personInfo.token;
        var uid = personInfo.uid;
        //领取积分
        wx.request({
          url: port.makeviplog, //请求地址
          header: { //请求头
            "Content-Type": "applciation/json",
            "Authorization": "Bearer " + token
          },
          data: {
            uid: uid,
            action: 'take_jf'
          },
          method: "post", //get为默认方法/POST
          success: function (res) {
            if (res.data.status == 'success') {
              wx.showToast({
                title: '积分领取成功~',
                icon: 'none'
              })
              that.setData({
                is_task: false,
              })
            } else {
            }
          },
          fail: function (err) { }, //请求失败
          complete: function (suc) { //请求完成后执行的函数
          }
        })
      })
    }

  },
  // 滑动
  move: function(e) {
    let currentX = e.touches[0].pageX
    let currentY = e.touches[0].pageY
    let tx = currentX - this.data.lastX
    let ty = currentY - this.data.lastY

    if (ty < 0 && this.data.clickIcon == 1) {
      this.setData({
        clickIcon: 0
      })
    } else if (ty > 0 && this.data.clickIcon == 0) {
      this.setData({
        clickIcon: 1
      })
    }


    //将当前坐标进行保存以进行下一次计算
    this.data.lastX = currentX
    this.data.lastY = currentY
  },
  //支付请求
  payMoney: function() {
    var that = this;
    var payType = that.data.selected;
    var mealTypeId = that.data.id;
    if (mealTypeId == 1) {
      var mealType = 'month';
      var money = 9.9;
      var body = '会员充值-一月';
    } else if (mealTypeId == 2) {
      var mealType = 'season';
      var money = 27.9;
      var body = '会员充值-一季';
    } else if (mealTypeId == 3) {
      var mealType = 'year';
      var money = 99;
      var body = '会员充值-一年';
    }
    if (mealTypeId != 0) {
      if (payType == false) {
        wx.showToast({
          title: '请先选择支付方式哦~',
          icon: 'none'
        })
      } else {
        app.getUserInfo(function(personInfo) {
          var token = personInfo.token;
          var uid = personInfo.uid;
          payFor.payFor(token, uid, body, mealType, function(data) {
            wx.requestPayment({
              timeStamp: data.timeStamp,
              nonceStr: data.nonceStr,
              package: data.package,
              signType: data.signType,
              paySign: data.paySign,
              success: function(res) {
                var shuttleIndex = that.data.shuttleIndex;
                wx.switchTab({
                  url: shuttleIndex + '/myCenter',
                  success: function(e) {
                    var page = getCurrentPages().pop();
                    if (page == undefined || page == null) return;
                    page.onLoad();
                  }
                })
              }
            })
          })
        })
      }
    } else {
      wx.showToast({
        title: '请先选择一个会员套餐哦~',
        icon: 'none'
      })
    }

  },
  // 选择支付类型
  chooseBtn: function(e) {
    var that = this;
    if (that.data.selected == false) {
      that.setData({
        selected: true
      })
    } else {
      that.setData({
        selected: false
      })
    }
  },
  // 会员类型
  buyWhice: function(e) {
    var id = e.currentTarget.id
    this.setData({
      id: id
    })
  },
  // 动画
  clickTable: function() {
    if (this.data.clickIcon == 0) {
      var clickIcon = 1;
      this.setData({
        clickIcon: clickIcon
      })
    } else {
      var clickIcon = 0;
      this.setData({
        clickIcon: clickIcon
      })
    }

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})