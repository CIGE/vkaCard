/**
 * 分包message配置文件
 */

var messageShuttle = '../../../shuttle/pages';   //跳转分包shuttle统一路径
var messageCenter = '../../../center/pages';  //跳转分包center统一路径
var messageIndex = '../../../../pages/index';   //跳转主包统一路径

var message = {
  messageShuttle,
  messageCenter,
  messageIndex,
}
module.exports = message