var app = getApp();
const port = require('../../../../config.js');
const mainIndex = require('../../../../pages/index.js');
const message = require('../message.js');

Page({
  /**
   * 页面的初始数据
   */
  data: {
    messageShuttle: message.messageShuttle,   //跳转分包library统一路径
    messageCenter: message.messageCenter,  //跳转分包myCenter统一路径
    messageIndex: message.messageIndex,   //跳转主包统一路径
    imgPath: port.imgPath,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    wx.hideShareMenu();
    //用户id
    that.setData({
      cid: options.cid,
    })
    app.getUserInfo(function (personInfo) {
      var token = personInfo.token;
      var uid = personInfo.uid;
      //获取用户基本信息、更多信息、视频
      wx.request({
        url: port.papershow,//请求地址
        header: {//请求头
          "Content-Type": "applciation/json",
          "Authorization": "Bearer " + token
        },
        data: { 'id': that.data.cid, 'uid': uid },
        method: "post",
        success: function (res) {
          var card = res.data.data;
          that.setData({
            card: card,
            mobile: card.mobile,
            address: card.address,
            region: card.ssq.split('-'),
            bgImg: card.templet_sz.bigBg,
          })
          //加载中Logo展示
          var opacity = 1;
          var timer = setInterval(function () {
            opacity = opacity - 0.25;
            that.setData({
              opacity: opacity,
            })
            if (opacity < 0.5) {
              that.setData({
                wetherLoadding: 'none'
              })
            }
          }, 200)
        },
        fail: function (err) { },//请求失败
        complete: function (suc) { }//请求完成后执行的函数
      })
    })
  },
  goUpdateCard: function(){
    var that = this;
    wx.navigateTo({
      url: '../updateCard/updateCard?cid=' + that.data.cid,
    })
  },
  //拨打电话
  callPhone: function () {
    var that = this
    wx.makePhoneCall({
      phoneNumber: that.data.card.mobile //仅为示例，并非真实的电话号码
    })
  },
  //保存手机号
  downPhone: function () {
    var that = this
    wx.addPhoneContact({
      firstName: that.data.card.username,//联系人姓名  
      mobilePhoneNumber: that.data.card.mobile,//联系人手机号  
    })
  },

  // 头像放大
  headerImg: function () {
    var that = this;
    var imgs = that.data.card.avatar
    wx.previewImage({
      urls: [that.data.imgPath + imgs], // 需要预览的图片http链接列表
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // this.opacity();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  // opacity: function(){
  //   var opacity = 1;
  //   // var inter = setInterval(function () {
  //   //   opacity = opacity - 0.1;
  //   //   if (opacity <= 0) {
  //   //     clearInterval(inter)
  //   //   }
  //   //   that.setData({
  //   //     opacity: opacity
  //   //   })
  //   // }, 10)
  // },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    var img = that.data.user_imgs;
    var cid = that.data.card.id;
    var limit = that.data.limit;
    var tatol = that.data.tatol
    if (img.length < tatol) {
      app.getUserInfo(function (personInfo) {
        var token = personInfo.token;
        wx.request({
          url: port.CardMyImg,//请求地址
          header: {//请求头
            "Content-Type": "applciation/json",
            "Authorization": "Bearer " + token
          },
          data: { 'cid': cid, limit: limit },
          method: "post",//get为默认方法/POST
          success: function (res) {
            if (res.data.status == 'success') {
              for (var i = 0; i < res.data.data.length; i++) {
                img.push(res.data.data[i])
              }
              that.setData({
                user_imgs: img,
                limit: that.data.limit + 1,
                tatol: res.data.tatol
              })
            }
          }
        })
      })
    }
  },
  wffx: function () {
    wx.showToast({
      title: '此名片涉嫌违规 无法分享',
      icon: 'none',
      mask: true,
      success: function () {
        return false
      }
    })
  },
  
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    var that = this;
    var path = "/pages/index/preview?cid=" + that.data.card.id + "&my=0";;
    return {
      title: 'V咖片',
      path: path,
      success: function (res) {
      },
      fail: function (res) {
        // 转发失败
      }
    }
  },
})