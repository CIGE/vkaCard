var app = getApp();
var interval = null //倒计时函数
const port = require('../../../../config.js');
const messageCheck = require('../permessage/messageCheck.js')
const message = require('../message.js');


Page({

  /**
   * 页面的初始数据
   */
  data: {
    dataId: 1,
    vIfShow: false,
    imgAllIfShow: true,
    index: '',
    myId: 1,
    array: ['绑定微信手机号', '输入手机号'],
    tempFilePaths: [],
    region: [],
    card: [],
    tximg: '/images/jimo.png',
    avatar: '',
    showModal: '',
    allGoodsFilte: [],
    workType: '',
    ty: true,
    time: '获取验证码', //倒计时 
    currentTime: 60,
    codeShow: false,//是否显示获取验证码
    phone: 0,
    t: true,
    imgPath: port.imgPath,
    videoPlay: 'block',
    album: [],
    cid: '',
    imgArray: [],
    aid: '',
    openArr: [
      { name: 'all', value: '完全公开', checked: true, types: '1' },
      { name: 'half', value: '有申请即公开', checked: '', types: '2' },
      { name: 'each', value: '相互通过公开', checked: '', types: '3' },
    ],
    deleteHide: "hidden",
    deleteCancel: true,
    wheCross: "no",
    address: '',
    objectMultiArray: [  //机构
      {
        id: 0,
        name: '海创会'
      },
      {
        id: 1,
        name: '全国服务力私董会'
      },
    ],
    institution: '请选择所属机构(选填)', //机构名称
    institutioncolor: '#999',
    messageShuttle: message.messageShuttle,   //跳转分包library统一路径
    messageCenter: message.messageCenter,  //跳转分包myCenter统一路径
    messageIndex: message.messageIndex   //跳转主包统一路径
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.hideShareMenu();
    var that = this;
    //用户数据
    var mobile=options['手机'].split('-').join('');
    var address;
    if (options['地址']){
      address = options['地址'];
    }else{
      address = options['英文地址'];
    }

    that.setData({
      cid: options.cid,
      card: options,
      mobile: mobile,
      ty: false,
      address: address,
    })
  },

  radioChange: function (e) {
    var that = this;
    var value = e.detail.value;
    var openArr = new Array();
    openArr = that.data.openArr;
    for (var j = 0; j < openArr.length; j++) {
      if (openArr[j].value == value) {
        openArr[j].checked = 'true'
      } else {
        openArr[j].checked = ''
      }
    }
  },

  //监听电话号码变化
  mobileInput: function (e) {
    var that = this;
    if (that.data.mobile != e.detail.value) {
      that.setData({
        codeShow: true
      })
    } else {
      that.setData({
        codeShow: false
      })
    }
    that.setData({
      phone: e.detail.value
    })

  },
  // 验证码倒计时
  getCode: function (options) {
    var that = this;
    var currentTime = that.data.currentTime
    interval = setInterval(function () {
      currentTime--;
      that.setData({
        time: currentTime + '秒'
      })
      if (currentTime <= 0) {
        clearInterval(interval)
        that.setData({
          time: '重新发送',
          currentTime: 60,
          disabled: false
        })
      }
    }, 1000)
  },

  //获取验证码
  getVerificationCode() {
    var that = this
    var phone = that.data.phone;
    if (phone.length != 11) {
      wx.showToast({
        title: '请正确输入手机号',
        image: '/imgs/error.png',
        duration: 1500
      })
      return false;
    }
    app.getUserInfo(function (personInfo) {
      var token = personInfo.token;
      wx.request({
        url: port.CodeSMS,//请求地址
        header: {//请求头
          "Content-Type": "applciation/json",
          "Authorization": "Bearer " + token
        },
        data: {
          mobile: that.data.phone,
          uid: personInfo.uid
        },
        method: "post",//get为默认方法/POST
        success: function (res) {
          if (res.data.status == 'success') {
            wx.showToast({
              title: '发送成功',
              icon: 'success',
              duration: 1500
            })
            that.setData({
              disabled: true
            })
            that.getCode();
          } else {
            wx.showToast({
              title: res.data.message,
              image: '/images/error.png',
              duration: 1500
            })
            return false;
          }
        },
        fail: function (err) { },//请求失败
        complete: function (suc) {//请求完成后执行的函数

        }
      })
    })
  },
  //获取微信授权 获取手机号
  getPhoneNumber: function (e) {
    var that = this;
    if (e.detail.errMsg == 'getPhoneNumber:fail user deny') {
      wx.removeStorageSync('WxLoginUserInfo');
      wx.showToast({
        title: '授权失败啦，再来一下！',
        icon: 'none',
        mask: true,
        duration: 1500
      })
    } else {
      wx.removeStorageSync('WxLoginUserInfo');
    }
    app.getUserInfo(function (personInfo) {
      var token = personInfo.token;
      var uid = personInfo.uid;
      wx.request({
        url: port.wechatMobile,//请求地址
        header: {//请求头
          "Content-Type": "applciation/json",
          "Authorization": "Bearer " + token
        },
        data: { 'iv': e.detail.iv, 'encryptedData': e.detail.encryptedData, 'sessionkey': personInfo.sessionkey, 'uid': uid },
        method: "post",//get为默认方法/POST
        success: function (res) {
          if (res.data.status == 'success') {
            var data = JSON.parse(res.data.data);
            that.setData({
              mobile: data.phoneNumber,
              ty: false
            })
          } else {
            wx.showToast({
              title: '授权失败啦，再来一下！',
              icon: 'none',
              mask: true,
              duration: 1500
            })
          }
        },
        fail: function (err) { },//请求失败
        complete: function (suc) {//请求完成后执行的函数

        }
      })
    })

  },


  //基本信息表单提交
  jibenMsgSubmit: function (e) {
    var that = this;
    var datas = '';
    var username = e.detail.value.username.trim(); //姓名
    var mobile = e.detail.value.mobile; //手机
    var company = e.detail.value.company.trim(); //公司
    var duty = e.detail.value.duty.trim(); //职务
    var mailbox = e.detail.value.mailbox.trim(); //邮箱
    var ssq = e.detail.value.ssq; //省市区
    var address = e.detail.value.address.trim(); //详细地址
    var types = 1;
    var cid = that.data.cid;
    var src = that.data.src;
    var is_code = 0;//0微信授权手机号码，1自己输入手机号码
    var code = e.detail.value.code ? e.detail.value.code : 0;
    var url = '';
    var avatar = that.data.avatar;
    var codeShow = that.data.codeShow;
    var openArr = new Array();
    var institution = that.data.institution;
    openArr = that.data.openArr;
    if (institution == '请选择所属机构(选填)') {
      institution = '';
    }
    for (var i = 0; i < openArr.length; i++) {
      if (openArr[i].checked == 'true') {
        types = openArr[i].types;
      }
    }
    var result = messageCheck.messageCheck(avatar, username, mobile, code, codeShow, company, duty, address, ssq, port.imgPath);
    if (result == false) {
      return
    }
    if (!avatar) {
      avatar = 'new-background/logo_aboutus.png';
    }
    that.setData({
      t: false
    })
    wx.showLoading({
      title: '正在创建',
    })
    app.getUserInfo(function (personInfo) {
      var token = personInfo.token;
      var uid = personInfo.uid;
      var openid = personInfo.openid;
      url = port.CardAdd;

      if (src) {
        wx.uploadFile({
          url: port.upload, //仅为示例，非真实的接口地址
          filePath: src,
          name: 'file',
          formData: {
            uid: uid,
            type: 'avatar',
            cid: cid
          },
          success: function (e) {
            var data = JSON.parse(e.data)
            if (data.status == "success") {
              var data = JSON.parse(e.data);
              var avatar = data.data;
              var backgroundUrl = new Object();
              backgroundUrl.smallBg = port.imgPath + "new-background/newbg-2.png";
              // backgroundUrl.iconColor="white";
              backgroundUrl.bgImgColor = "white";
              backgroundUrl.bigBg = port.imgPath + "new-background/newbg-2long.png";
              backgroundUrl.h5Bg = port.imgPath + "new-background/bigbg3.jpg";
              backgroundUrl.h5Color = 'white';
              backgroundUrl.id_smallbg = "0";
              backgroundUrl.id_h5bg = '3';
              datas = {
                'username': username,
                'mobile': mobile,
                'company': company,
                'duty': duty,
                'mailbox': mailbox,
                'ssq': ssq,
                'address': address,
                'types': types,
                'avatar': avatar,
                'is_code': is_code,
                'code': code,
                'templet_sz': backgroundUrl,
                // 'institution': institution
              }
              wx.request({
                url: url,//请求地址
                header: {//请求头
                  "Content-Type": "applciation/json",
                  "Authorization": "Bearer " + token
                },
                data: { 'data': datas, 'uid': uid, 'cid': cid, 'type': 1 },
                method: "post",//get为默认方法/POST
                success: function (res) {
                  if (res.data.status == 'success') {
                    wx.hideLoading();
                    wx.showToast({
                      title: '保存成功',
                      icon: 'success',
                      mask: true,
                      duration: 1500,
                      success: function () {
                        var messageIndex = that.data.messageIndex;
                        setTimeout(function () {
                          wx.reLaunch({
                            url: messageIndex + "/myCard?id=" + res.data.data,
                            success: function () { },
                            fail: function () { },
                            complete: function () { }
                          })
                        }, 1000)
                      }
                    })

                  } else {
                    that.setData({
                      t: true
                    })
                    wx.showToast({
                      title: res.data.message,
                      icon: 'none',
                      mask: true,
                      duration: 1500
                    })
                  }
                },
                fail: function (err) { },//请求失败
                complete: function (suc) {//请求完成后执行的函数
                }
              })
            } else {

            }
          }
          ,
          faill: function (res) {
          }
        })
      } else {
        var backgroundUrl = new Object();
        backgroundUrl.smallBg = port.imgPath + "new-background/newbg-2.png";
        // backgroundUrl.iconColor="white";
        backgroundUrl.bgImgColor = "white";
        backgroundUrl.bigBg = port.imgPath + "new-background/newbg-2long.png";
        backgroundUrl.h5Bg = port.imgPath + "new-background/bigbg3.jpg";
        backgroundUrl.h5Color = 'white';
        backgroundUrl.id_smallbg = "0";
        backgroundUrl.id_h5bg = '3';
        datas = {
          'username': username,
          'mobile': mobile,
          'company': company,
          'duty': duty,
          'mailbox': mailbox,
          'ssq': ssq,
          'address': address,
          'types': types,
          'avatar': avatar,
          'is_code': is_code,
          'code': code,
          'templet_sz': backgroundUrl,
          // 'institution': institution
        }
        wx.request({
          url: url,//请求地址
          header: {//请求头
            "Content-Type": "applciation/json",
            "Authorization": "Bearer " + token
          },
          data: { 'data': datas, 'uid': uid, 'cid': cid, 'type': 1 },
          method: "post",//get为默认方法/POST
          success: function (res) {
            if (res.data.status == 'success') {

              wx.showToast({
                title: '保存成功',
                icon: 'success',
                mask: true,
                duration: 1500,
                success: function () {
                  var messageIndex = that.data.messageIndex;
                  setTimeout(function () {
                    wx.reLaunch({
                      url: messageIndex + "/myCard?id=" + res.data.data,
                      success: function () { },
                      fail: function () { },
                      complete: function () { }
                    })
                  }, 1000)
                }
              })
            } else {
              that.setData({
                t: true
              })
              wx.showToast({
                title: res.data.message,
                icon: 'none',
                mask: true,
                duration: 1500
              })
            }
          },
          fail: function (err) { },//请求失败
          complete: function (suc) {//请求完成后执行的函数

          }
        })
      }

    })
  },

  //选择手机或者微信手机号
  pickerPhone: function (e) {
    var that = this;
    console.log(e.detail.value)
    if (e.detail.value == 0) {
      that.setData({
        ty: true,
        codeShow: false
      })
    } else if (e.detail.value == 1) {
      that.setData({
        ty: false,
      })
      if (that.data.cid == 0) {
        that.setData({
          codeShow: true
        })
      }
    }

  },

  back: function () {
    var that = this;
    wx.navigateBack({
      delta: 1
    })
  },

  backCenter: function () {
    var that = this;
    var messageIndex = that.data.messageIndex;
    wx.switchTab({
      url: messageIndex + '/myCenter',
    })
  },

  //地址选择
  bindRegionChange: function (e) {
    this.setData({
      region: e.detail.value
    })
  },
  //这里是选取图片的方法
  choose: function () {
    var that = this;

    wx.chooseImage({
      count: 1,
      success: function (res) {

        var tempFilePaths = res.tempFilePaths
        that.setData({
          tximg: tempFilePaths[0],
          src: tempFilePaths[0],
          avatar: tempFilePaths[0],
        })
      },
      faill: function (res) {
      }
    })

  },
  //获取当前位置
  getLocation: function () {
    var that = this;
    wx.chooseLocation({
      success: function (res) {
        that.setData({
          address: res.name
        })
      },
    })
  },
  //选择机构
  bindMultiPickerChange: function (option) {
    var that = this;
    var value_id = option.detail.value;
    var objectMultiArray = new Array();
    objectMultiArray = that.data.objectMultiArray;
    for (var i = 0; i < objectMultiArray.length; i++) {
      if (objectMultiArray[i].id == value_id) {
        var value = objectMultiArray[i].name;
        that.setData({
          institution: value,
          institutioncolor: 'black'
        })
      }
    }
  },
  getuserinfo: function(e){
    var that = this;
    var result = e.detail.errMsg;
    if (result == 'getUserInfo:ok'){
      var tximg = e.detail.userInfo.avatarUrl;
      that.setData({
        tximg: tximg
      })
    }
    app.getUserInfo(function (personInfo) {
      var token = personInfo.token;
      var uid = personInfo.uid;
      var userinfo = new Array();
      userinfo = e.detail.userInfo;
      wx.request({
        url: port.setuserinfo,//请求地址
        header: {//请求头
          "Content-Type": "applciation/json",
          "Authorization": "Bearer " + token
        },
        data: { 'userinfo': userinfo , 'uid': uid},
        method: "post",//get为默认方法/POST
        success: function (res) {
          if (res.data.status == 'success') {

          } else {
            that.setData({
              t: true
            })
            wx.showToast({
              title: res.data.message,
              icon: 'none',
              mask: true,
              duration: 1500
            })
          }
        },
        fail: function (err) { },//请求失败
        complete: function (suc) {//请求完成后执行的函数

        }
      })
    })

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function (res) {
    this.videoContext = wx.createVideoContext("video1");
  },
  inputValue: '',
  bindInputBlur: function (e) {
    this.inputValue = e.detail.value
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function (options) {
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})