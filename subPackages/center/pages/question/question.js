var app = getApp();
const port = require('../../../../config.js');
const center = require('../center.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    selected: 0,
    message_title:"",
    message_content:"",
    anonymous:0,
    centerShuttle: center.centerShuttle,   //跳转分包shuttle统一路径
    centerMessage: center.centerMessage,  //跳转分包message统一路径
    centerIndex: center.centerIndex   //跳转主包统一路径
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.hideShareMenu();
  },
  radioChange: function () {
    var that = this;
    if (that.data.selected == 1) {
      that.setData({
        selected: 0
      })
    } else {
      that.setData({
        selected: 1
      })
    }

  },
  no:function(){
    wx.switchTab({
      url: "../../myCenter/myCenter"
    })
  },
  sub: function (e) {
    var message_title = e.detail.value.tit;
    var message_content = e.detail.value.adviceCon;
    if (message_title == "" || message_content == ""){
      wx.showToast({
        title: '内容不为空',
        icon: 'loading',
        duration: 1000,
      })
    }else{
      if (this.data.selected == 0) {
        var anonymous = 0
      } else {
        var anonymous = 1
      }
      this.setData({
        message_title: message_title,
        message_content: message_content,
        anonymous: anonymous
      })
      var that = this;
      app.getUserInfo(function (personInfo) {
        var token = personInfo.token;
        var openid = personInfo.openId;
        wx.request({
          url: port.messageboard,//请求地址
          header: {//请求头
            "Content-Type": "applciation/json",
            "Authorization": "Bearer " + token
          },
          method: "post",//get为默认方法/POST
          data: {
            openid: openid,
            message_title: message_title,
            message_content: message_content,
            anonymous: anonymous
          },
          success: function (res) {
            wx.showToast({
              title: '提交成功',
              icon: 'success',
              duration: 1000,
            })
            setTimeout(function () {
              wx.switchTab({
                url: "../../myCenter/myCenter"
              })
            }, 2000)
            
          },
          fail: function (err) {
            wx.showToast({
              title: '提交失败',
              icon: 'success',
              duration: 1000
            })
          },//请求失败
          complete: function (suc) {//请求完成后执行的函数
          }
        })
      })
    } 
    
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})