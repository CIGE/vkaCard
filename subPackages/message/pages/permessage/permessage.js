var app = getApp();
var interval = null //倒计时函数
const port = require('../../../../config.js');
const messageCheck = require('messageCheck.js');
const message = require('../message.js');
const judgeVip = require('../../../../common/judgeVip.js');

Page({

  /*
   * 页面的初始数据
   */
  data: {
    dataId: 1,
    vIfShow: false,
    imgAllIfShow: true,
    cancelAlbumn: true, //是否超过一个相册
    index: '',
    myId: 1,
    array: ['绑定微信手机号', '输入手机号'],
    tempFilePaths: [],
    region: [],
    card: [],
    tximg: '',
    avatar: '',
    showModal: '',
    allGoodsFilte: [],
    workType: '',
    ty: true,
    time: '获取验证码', //倒计时 
    currentTime: 60,
    codeShow: false, //是否显示获取验证码
    phone: 0,
    t: true,
    imgPath: port.imgPath,
    videoPlay: 'block',
    album: [],
    caLength: 0,
    cid: '',
    imgArray: [],
    aid: '',
    openArr: [{
        name: 'all',
        value: '完全公开',
        checked: '',
        types: '1'
      },
      {
        name: 'half',
        value: '有申请即公开',
        checked: '',
        types: '2'
      },
      {
        name: 'each',
        value: '相互通过公开',
        checked: '',
        types: '3'
      },
    ],
    deleteHide: "hidden",
    deleteCancel: true,
    wheCross: "no",
    personImg: [],
    zhezhaoc: false,
    textareaCeng: true,
    // author: zhoujing
    ziyuanArr: [],
    needArr: [],
    objectMultiArray: [ //机构
      {
        id: 0,
        name: '海创会'
      },
      {
        id: 1,
        name: '全国服务力私董会'
      },
    ],
    institution: '请选择所属机构(选填)', //机构名称
    institutioncolor: '#999',
    myziyuan: '', //我的资源
    myxuqiu: '', //我的需求
    perCheck: 0, //判断是否第一次进入
    prompt: 1,
    messageShuttle: message.messageShuttle, //跳转分包library统一路径
    messageCenter: message.messageCenter, //跳转分包myCenter统一路径
    messageIndex: message.messageIndex, //跳转主包统一路径
    is_vip: false, //是否是会员
    picLimit: 8, //照片上限
    vipTip: false, //成为会员弹窗

  },

  /*
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    wx.hideShareMenu();
    var that = this;
    //跳转哪个tab页
    if (options.dataId) {
      that.setData({
        dataId: options.dataId
      })
    }
    //用户id
    that.setData({
      cid: options.cid
    })
    app.getUserInfo(function(personInfo) {
      var token = personInfo.token;
      var uid = personInfo.uid;
      //获取用户基本信息、更多信息、视频
      wx.request({
        url: port.CardMy, //请求地址
        header: { //请求头
          "Content-Type": "applciation/json",
          "Authorization": "Bearer " + token
        },
        data: {
          'uid': uid
        },
        method: "post", //get为默认方法/POST
        success: function(res) {
          if (res.data.status == 'success') {
            for (var i in res.data.data) {
              if (res.data.data[i] == null)
                res.data.data[i] = ''
            }
            // 设置关键词
            that.setData({
              ziyuanArr: res.data.data.keyword1,
              needArr: res.data.data.keyword2
            })
            var cid = options.cid
            if (res.data.data.institution != '') { //判断机构是否为空
              var institution = res.data.data.institution;
              var institutioncolor = 'black';
            } else {
              var institution = that.data.institution;
              var institutioncolor = '#999';
            }
            if (res.data.data.avatar == "") {
              var tximg = res.data.data.avatar;
            } else {
              var tximg = port.imgPath + res.data.data.avatar
            }
            that.setData({
              card: res.data.data,
              institution: institution,
              institutioncolor: institutioncolor,
              mobile: res.data.data.mobile,
              tximg: tximg,
              avatar: res.data.data.avatar,
              region: res.data.data.ssq.split("-"),
              workType: res.data.data.industry,
              ty: false,
              cid: cid
            })
            var types = res.data.data.types;
            var openArr = new Array();
            openArr = that.data.openArr;
            for (var i = 0; i < openArr.length; i++) {
              if (openArr[i].types == types) {
                openArr[i].checked = 'true';
              }
            }
            that.setData({
              openArr: openArr,
            })

            if (res.data.data.video != '' && res.data.data.video != null) {
              var video = JSON.parse(res.data.data.video);
              that.setData({
                title: video.title,
                video: that.data.imgPath + video.video,
                video1: video.video,
                vIfShow: false,
              })
            } else {
              that.setData({
                vIfShow: true,
                videoPlay: "none"
              })
            }
          }
        },
        fail: function(err) {
          wx.hideLoading()
        }, //请求失败
        complete: function(suc) { //请求完成后执行的函数

        }
      })

      //获取用户相册
      wx.request({
        url: port.CardAlbumList,
        method: 'get',
        data: {
          cid: that.data.cid
        },
        header: {
          'content-type': 'application/json', // 默认值
          "Authorization": "Bearer " + personInfo.token
        },
        success: function(res) {
          if (res.data.status == "success") {
            //初始化相册
            var personImg = new Array()
            for (var i = 0; i < res.data.data.length; i++) {
              var personImgO = new Object();
              personImgO.id = i;
              personImgO.imgIfShow = "none";
              personImgO.img = "../../imgs/user_img.jpg"
              personImgO.imgsName = "相册一";
              personImgO.imgNum = 50;
              personImgO.SendImgTime = "2018-12-12";
              personImg.push(personImgO);
              res.data.data[i].updated_at = res.data.data[i].updated_at.split(" ")[0]
            }
            that.setData({
              album: res.data.data,
              hidden: false,
              personImg: personImg
            })
            if (res.data.data.length >= 1) {
              that.setData({
                cancelAlbumn: false,
              })
            }
          } else {
            that.setData({
              album: [],
              hidden: true
            })
          }
        }
      })

      //行业列表
      app.getUserInfo(function(personInfo) {
        var token = personInfo.token;
        wx.request({
          url: port.CardIndustry, //请求地址
          header: { //请求头
            "Content-Type": "applciation/json",
            "Authorization": "Bearer " + token
          },
          method: "get", //get为默认方法/POST
          success: function(res) {
            if (res.data.status == 'success') {
              that.setData({
                allGoodsFilte: res.data.data
              })
            }
          },
          fail: function(err) {}, //请求失败
          complete: function(suc) { //请求完成后执行的函数

          }
        })
      })

      //判断会员
      judgeVip.judgeVip(token, uid, function(result) {
        if (result) {
          that.setData({
            is_vip: true,
            picLimit: 20
          })
        } else {
          that.setData({
            is_vip: false,
            picLimit: 8,
          })
        }
      })
    })
  },

  radioChange: function(e) {
    var that = this;
    var value = e.detail.value;
    var openArr = new Array();
    openArr = that.data.openArr;
    for (var j = 0; j < openArr.length; j++) {
      if (openArr[j].value == value) {
        openArr[j].checked = 'true'
      } else {
        openArr[j].checked = ''
      }
    }
  },

  //跳转会员中心
  vipCenter: function() {
    var that = this;
    var cid = that.data.cid
    if (!cid) {
      wx.showToast({
        icon: 'success',
        image: '/images/error.png',
        title: '请先添加名片',
      })
    } else {
      var messageShuttle = that.data.messageShuttle;
      wx.navigateTo({
        url: messageShuttle + '/vipCenter/vipCenter',
      })
    }
  },

  //行业
  showDialogBtn: function() {
    this.setData({
      showModal: true,
      textareaCeng: false,
    })
  },
  hideModal: function() {
    this.setData({
      showModal: false
    });
  },
  serviceValChange: function(e) {
    var allGoodsFilte = this.data.allGoodsFilte;
    var checkArr = e.detail.value;
    var a = checkArr[0] - 1
    var b = checkArr[1] - 1
    var c = checkArr[2] - 1
    if (checkArr.length == 1) {
      this.setData({
        workType: allGoodsFilte[a].name
      })
    }
    if (checkArr.length == 2) {
      this.setData({
        workType: allGoodsFilte[a].name + ',' + allGoodsFilte[b].name
      })
    }
    if (checkArr.length == 3) {
      this.setData({
        workType: allGoodsFilte[a].name + ',' + allGoodsFilte[b].name + ',' + allGoodsFilte[c].name
      })
    }
    if (checkArr.length > 3) {
      wx.showModal({
        title: '提示',
        content: '最多只能选3个行业！',
        showCancel: false,
        success: function(res) {
          if (res.confirm) {
            console.log('用户点击确定')
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
    }
    for (var i = 1; i <= allGoodsFilte.length; i++) {
      if (checkArr.indexOf(i + "") != -1) {
        if (checkArr.length > 3) {
          allGoodsFilte[3].checked = false;
        } else {
          allGoodsFilte[i - 1].checked = true;
        }
      } else {
        allGoodsFilte[i - 1].checked = false;
      }
    }
    this.setData({
      allGoodsFilte: allGoodsFilte
    })
  },
  /*
   * 对话框取消按钮点击事件
   */
  onCancel: function() {
    this.hideModal();
    this.setData({
      textareaCeng: true
    })
  },
  /*
   * 对话框确认按钮点击事件
   */
  onConfirm: function() {
    this.hideModal();
    this.setData({
      textareaCeng: true
    })
  },


  //导航栏切换
  info: function(e) {
    var that = this;
    var cid = that.data.card.id;
    var dataId = e.currentTarget.dataset.id;
    this.setData({
      dataId: dataId
    })
    //wangwj
    that.setData({
      cid: cid
    })
    if (cid > 0) {
      app.getUserInfo(function(personInfo) {
        var token = personInfo.token;
        var uid = personInfo.uid;
        //重新调取用户基本信息、更多信息、视频
        wx.request({
          url: port.CardMy, //请求地址
          header: { //请求头
            "Content-Type": "applciation/json",
            "Authorization": "Bearer " + token
          },
          data: {
            'cid': cid,
            'uid': uid
          },
          method: "post", //get为默认方法/POST
          success: function(res) {
            if (res.data.status == 'success') {
              if (res.data.data.avatar == "") {
                var tximg = res.data.data.avatar;
              } else {
                var tximg = port.imgPath + res.data.data.avatar
              }
              that.setData({
                card: res.data.data,
                mobile: res.data.data.mobile,
                tximg: tximg,
                avatar: res.data.data.avatar,
                region: res.data.data.ssq.split("-"),
                workType: res.data.data.industry,
                ty: false,
              })
            }
          },
          fail: function(err) {}, //请求失败
          complete: function(suc) { //请求完成后执行的函数

          }
        })
      })
    }
  },
  //开始播放视频
  vPlay: function(e) {
    this.videoContext.play()
  },
  //暂停视频执行的操作
  showvAddImg: function() {
    this.setData({
      videoPlay: 'block'
    })
  },
  //开始播放视频时执行的操作
  hidvAddImg: function() {
    this.setData({
      videoPlay: 'none'
    })
  },
  //展示相册内相片
  showImgs: function(e) {
    var that = this;
    var aid = e.currentTarget.dataset.aid
    var id = e.currentTarget.dataset.id;
    var myId = id;
    var personImg = new Array();
    personImg = that.data.personImg;
    personImg[id].imgIfShow = "block"
    that.setData({
      imgAllIfShow: false,
      personImg: personImg,
      id: id,
      myId: myId,
      aid: aid,
      wheCross: "ok",
    })
    //请求相册数据
    if (aid != null) {
      app.getUserInfo(function(personInfo) {
        wx.request({
          url: port.CardAlbuminfo,
          method: 'get',
          data: {
            aid: aid
          },
          header: {
            'content-type': 'application/json', // 默认值
            "Authorization": "Bearer " + personInfo.token
          },
          success: function(res) {
            if (res.data.status == 'success') {
              that.setData({
                title: res.data.data.title,
                imgArray: res.data.data.img,
                caLength: res.data.data.img.length,
              })
            } else {
              wx.showToast({
                title: res.data.message,
                image: '../../imgs/error.png',
                mask: true,
                duration: 2000,
                success: function(r) {}
              })
            }
          }
        })
      })
    } else {
      that.setData({
        cid: options.cid,
        disable: false,
      })
    }
  },
  addAlbum: function(e) {
    var that = this;
    var id = this.data.personImg.length;
    var myId = id;
    var personImg = new Array();
    personImg = this.data.personImg;
    this.setData({
      imgAllIfShow: false,
      personImg: personImg,
      id: id,
      myId: myId,
      title: '',
      wheCross: "no"
    })
    //请求相册数据

  },
  ablumTitle: function(e) {
    var that = this;
    that.setData({
      title: e.detail.value
    })
  },
  //長按圖片操作
  // bingLongTap: function (e) {
  //   var that = this;
  //   that.setData({
  //     deleteHide: "visible",
  //     deleteCancel: false
  //   })
  // },

  //點擊取消圖片操作
  // deleteCancelFun: function(e){
  //   var that = this;
  //   that.setData({
  //     deleteHide: "hidden",
  //     deleteCancel: true
  //   })
  // },
  //点击图片预览
  previewImage: function(e) {
    var that = this;
    var current = e.currentTarget.dataset.src;
    var numId = e.currentTarget.id;
    if (e.currentTarget.dataset.class == 'whichImg') {
      var imgString = that.data.imgPath + that.data.album[numId].img;
      var imgString = that.data.album[numId].img;
      var imgList = new Array();
      for (var i = 0; i < imgString.length; i++) {
        var list = that.data.imgPath + imgString[i]
        imgList.push(list)
      }
    } else if (e.currentTarget.dataset.class == 'imgList') {
      var imgList = new Array();
      imgList = that.data.tempFilePaths;
    }

    wx.previewImage({
      current: current,
      urls: imgList,
    })
  },
  //关闭成为vip提示
  cancelVipTip: function() {
    var that = this;
    that.setData({
      vipTip: false
    })
  },
  //点击添加图片
  addImg: function(e) {
    var that = this;
    var tempFilePaths = [];
    var arry = [];
    var addimgCount = 0;
    var id = that.data.id;
    var caLength = that.data.caLength;
    wx.chooseImage({
      count: 50, // 默认9
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success: function(res) {
        tempFilePaths = res.tempFilePaths;
        arry = that.data.tempFilePaths.concat(tempFilePaths);
        addimgCount = arry.length;
        var picLimit = that.data.picLimit;
        var is_vip = that.data.is_vip;
        if (addimgCount + caLength > picLimit) {
          if (picLimit == 8 || is_vip == false) {
            that.setData({
              vipTip: true,
            })
          } else {
            wx.showToast({
              icon: "none",
              title: '您的权限只能上传' + picLimit + '张哦',
              duration: 900,
            })
          }

        } else {
          that.setData({
            tempFilePaths: arry
          })
          if (that.data.aid) {
            that.uploadimg({
              url: port.upload, //这里是你图片上传的接口
              path: tempFilePaths, //这里是选取的图片的地址数组
              aid: that.data.aid
            });
          } else {
            that.setData({
              tempFilePaths: arry
            })
          }
        }
      }
    })
    that.setData({
      caLength: that.data.caLength + addimgCount,
    })
  },

  //删除相片
  delateImg: function(event) {
    var that = this;
    wx.showModal({
      title: '温馨提示',
      content: '确定要删除此张照片吗？',
      success: function(res) {
        if (res.confirm) {
          app.getUserInfo(function(personInfo) {
            wx.request({
              url: port.FileDel, //仅为示例，并非真实的接口地址
              method: 'post',
              data: {
                filename: event.currentTarget.dataset.imgpath,
                aid: that.data.aid,
                type: 'album'
              },
              header: {
                'content-type': 'application/json', // 默认值
                "Authorization": "Bearer " + personInfo.token
              },
              success: function(res) {
                if (res.data.code == 200) {
                  wx.showToast({
                    title: '删除成功',
                    icon: 'success',
                    mask: true,
                    duration: 900
                  })
                  setTimeout(function() {
                    var imglist = that.data.imgArray;
                    imglist.splice(event.currentTarget.id, 1)
                    that.setData({
                      imgArray: imglist
                    })
                  }, 900)
                } else {
                  wx.showToast({
                    title: res.data.message,
                    icon: 'success',
                    mask: true,
                    duration: 2000
                  })
                }
              }
            })
          });
          that.setData({
            caLength: that.data.caLength - 1,
          });
        } else if (res.cancel) {}
      }
    })
  },
  //删除本地照片
  delatelocalhostImg: function(event) {
    var that = this;
    wx.showModal({
      title: '温馨提示',
      content: '确定要删除此张照片吗？',
      success: function(res) {
        if (res.confirm) {
          wx.showToast({
            title: '删除成功',
            icon: 'success',
            mask: true,
            duration: 1500
          })
          setTimeout(function() {
            var imglist = that.data.tempFilePaths;
            imglist.splice(event.currentTarget.id, 1)
            that.setData({
              tempFilePaths: imglist
            })
          }, 1500)
        } else if (res.cancel) {}
      }
    })
  },

  //上传图片
  uploadimg: function(data) {
    wx.showLoading({
      title: '上传中',
      mask: true,
    })
    var that = this,
      i = data.i ? data.i : 0, //当前上传的哪张图片
      success = data.success ? data.success : 0, //上传成功的个数
      fail = data.fail ? data.fail : 0, //上传失败的个数,
      aid = data.aid ? data.aid : 0,
      mold = data.mold ? data.mold : 1;
    var imgArray = data.path;
    app.getUserInfo(function(personInfo) {
      var uid = personInfo.uid;
      wx.uploadFile({
        url: data.url,
        filePath: data.path[i],
        name: 'file', //这里根据自己的实际情况改
        formData: {
          type: 'album',
          uid: uid,
          aid: data.aid
        },
        success: (resp) => {
          var data = JSON.parse(resp.data)
          if (data.status == 'success') {
            success++; //图片上传成功，图片上传成功的变量+1
            var imglist = that.data.imgArray;
            //判断是多创建相册还是做单张上传
            if (mold == 1) {
              setTimeout(function() {
                // imglist.push(data.data)
                // that.setData({
                //   imgArray: imglist,
                // })
                wx.hideLoading()
              }, 2000)
            } else {}
          } else {
            fail++
            wx.showModal({
              title: '提示',
              content: data.message,
            })
          }
          //这里可能有BUG，失败也会执行这里,所以这里应该是后台返回过来的状态码为成功时，这里的success才+1
        },
        fail: (res) => {
          wx.hideLoading()
          fail++; //图片上传失败，图片上传失败的变量+1
          setTimeout(function() {
            wx.showToast({
              title: '上传失败',
              icon: 'loading',
              mask: true,
              duration: 1500
            })
          }, 1500)
        },
        complete: () => {
          i++; //这个图片执行完上传后，开始上传下一张
          if (i == data.path.length) { //当图片传完时，停止调用          
            if (mold == 2) {
              wx.hideLoading()
              setTimeout(function() {
                wx.showToast({
                  title: '创建成功',
                  icon: 'success',
                  mask: true,
                  duration: 2000
                })
                setTimeout(function() {
                  wx.navigateBack({
                    delta: 1
                  })
                }, 2000)
              }, 1000)
            }
          } else { //若图片还没有传完，则继续调用函数
            data.i = i;
            data.success = success;
            data.fail = fail;
            if (mold == 1) {
              setTimeout(function() {
                that.uploadimg(data);
              }, 1500)
            } else {
              that.uploadimg(data);
            }
          }
        }
      });
    })
  },

  //监听电话号码变化
  mobileInput: function(e) {
    var that = this;
    if (that.data.card.id > 0) {
      if (that.data.mobile != e.detail.value) {
        that.setData({
          codeShow: true
        })
      } else {
        that.setData({
          codeShow: false
        })
      }
    }
    that.setData({
      phone: e.detail.value
    })

  },
  // 验证码倒计时
  getCode: function(options) {
    var that = this;
    var currentTime = that.data.currentTime
    interval = setInterval(function() {
      currentTime--;
      that.setData({
        time: currentTime + '秒'
      })
      if (currentTime <= 0) {
        clearInterval(interval)
        that.setData({
          time: '重新发送',
          currentTime: 60,
          disabled: false
        })
      }
    }, 1000)
  },

  //获取验证码
  getVerificationCode() {
    var that = this
    var phone = that.data.phone;
    if (phone.length != 11) {
      wx.showToast({
        title: '请正确输入手机号',
        image: '/imgs/error.png',
        duration: 1500
      })
      return false;
    }
    app.getUserInfo(function(personInfo) {
      var token = personInfo.token;
      wx.request({
        url: port.CodeSMS, //请求地址
        header: { //请求头
          "Content-Type": "applciation/json",
          "Authorization": "Bearer " + token
        },
        data: {
          mobile: that.data.phone,
          uid: personInfo.uid
        },
        method: "post", //get为默认方法/POST
        success: function(res) {
          if (res.data.status == 'success') {
            wx.showToast({
              title: '发送成功',
              icon: 'success',
              duration: 1500
            })
            that.setData({
              disabled: true
            })
            that.getCode();
          } else {
            wx.showToast({
              title: res.data.message,
              image: '/imgs/error.png',
              duration: 1500
            })
            return false;
          }
        },
        fail: function(err) {}, //请求失败
        complete: function(suc) { //请求完成后执行的函数

        }
      })
    })
  },
  //获取微信授权 获取手机号
  getPhoneNumber: function(e) {
    var that = this;
    wx.removeStorageSync('WxLoginUserInfo');
    if (e.detail.errMsg == 'getPhoneNumber:fail user deny') {
      wx.showToast({
        title: '授权失败',
        icon: 'none',
        mask: true,
        duration: 1500
      })
    } else {
      app.getUserInfo(function(personInfo) {
        var token = personInfo.token;
        var uid = personInfo.uid;
        wx.request({
          url: port.wechatMobile, //请求地址
          header: { //请求头
            "Content-Type": "applciation/json",
            "Authorization": "Bearer " + token
          },
          data: {
            'iv': e.detail.iv,
            'encryptedData': e.detail.encryptedData,
            'sessionkey': personInfo.sessionkey,
            'uid': uid
          },
          method: "post", //get为默认方法/POST
          success: function(res) {
            if (res.data.status == 'success') {
              var data = JSON.parse(res.data.data);
              that.setData({
                mobile: data.phoneNumber,
                ty: false
              })
            } else {
              wx.showToast({
                title: '授权失败',
                icon: 'none',
                mask: true,
                duration: 1500
              })
            }
          },
          fail: function(err) {}, //请求失败
          complete: function(suc) { //请求完成后执行的函数

          }
        })
      })
    }
  },


  //基本信息表单提交
  jibenMsgSubmit: function(e) {
    var that = this;
    var datas = '';
    var username = e.detail.value.username.trim(); //姓名
    var mobile = e.detail.value.mobile; //手机
    var company = e.detail.value.company.trim(); //公司
    var duty = e.detail.value.duty.trim(); //职务
    var mailbox = e.detail.value.mailbox.trim(); //邮箱
    var ssq = e.detail.value.ssq; //省市区
    var address = e.detail.value.address.trim(); //详细地址
    var types = ''; //隐私设置
    var cid = that.data.cid;
    var src = that.data.src;
    var is_code = 0; //0微信授权手机号码，1自己输入手机号码
    var code = e.detail.value.code ? e.detail.value.code : 0;
    var url = '';
    var openArr = new Array();
    var avatar = that.data.avatar;
    var codeShow = that.data.codeShow;
    var institution = that.data.institution;
    openArr = that.data.openArr;
    if (institution == '请选择所属机构(选填)') {
      institution = '';
    }
    for (var i = 0; i < openArr.length; i++) {
      if (openArr[i].checked == 'true') {
        types = openArr[i].types;
      }
    }
    var result = messageCheck.messageCheck(avatar, username, mobile, code, codeShow, company, duty, address, ssq, port.imgPath);
    if (result == false) {
      return
    }

    that.setData({
      t: false
    })
    app.getUserInfo(function(personInfo) {
      var token = personInfo.token;
      var uid = personInfo.uid;
      var openid = personInfo.openId;
      if (cid == 0) {
        url = port.CardAdd;
      } else {
        url = port.CardUpdate;
      }
      if (src) {
        wx.uploadFile({
          url: port.upload,
          filePath: src,
          name: 'file',
          formData: {
            uid: uid,
            type: 'avatar',
            cid: cid
          },
          success: function(e) {
            var data = JSON.parse(e.data);
            var avatar = data.data;
            datas = {
              'username': username,
              'mobile': mobile,
              'company': company,
              'duty': duty,
              'mailbox': mailbox,
              'ssq': ssq,
              'address': address,
              'types': types,
              'avatar': avatar,
              'is_code': is_code,
              'code': code,
              // 'institution': institution
            }
            wx.request({
              url: url, //请求地址
              header: { //请求头
                "Content-Type": "applciation/json",
                "Authorization": "Bearer " + token
              },
              data: {
                'data': datas,
                'uid': uid,
                'cid': cid,
                'type': 1
              },
              method: "post", //get为默认方法/POST
              success: function(res) {
                if (res.data.status == 'success') {
                  wx.showToast({
                    title: '保存成功',
                    icon: 'success',
                    mask: true,
                    duration: 1500,
                    success: function() {
                      var messageIndex = that.data.messageIndex;
                      setTimeout(function() {
                        wx.reLaunch({
                          url: messageIndex + "/myCard?id=" + res.data.data,
                          success: function() {},
                          fail: function() {},
                          complete: function() {}
                        })
                      }, 1000)
                    }
                  })

                } else {
                  that.setData({
                    t: true
                  })
                  wx.showToast({
                    title: res.data.message,
                    icon: 'none',
                    mask: true,
                    duration: 1500
                  })
                }
              },
              fail: function(err) {}, //请求失败
              complete: function(suc) { //请求完成后执行的函数

              }
            })

          },
          faill: function(res) {}
        })
      } else {
        var avatar = that.data.avatar
        datas = {
          'username': username,
          'duty': duty,
          'company': company,
          'mobile': mobile,
          'mailbox': mailbox,
          'address': address,
          'avatar': avatar,
          'is_code': is_code,
          'code': code,
          'ssq': ssq,
          'types': types,
          // 'institution': institution
        }
        wx.request({
          url: url, //请求地址
          header: { //请求头
            "Content-Type": "applciation/json",
            "Authorization": "Bearer " + token
          },
          data: {
            'data': datas,
            'uid': uid,
            'cid': cid,
            'type': 1
          },
          method: "post", //get为默认方法/POST
          success: function(res) {
            if (res.data.status == 'success') {
              wx.showToast({
                title: '保存成功',
                icon: 'success',
                mask: true,
                duration: 1500,
                success: function() {
                  var messageIndex = that.data.messageIndex;
                  setTimeout(function() {
                    wx.reLaunch({
                      url: messageIndex + "/myCard?id=" + res.data.data,
                      success: function() {},
                      fail: function() {},
                      complete: function() {}
                    })
                  }, 1000)
                }
              })
            } else {
              that.setData({
                t: true
              })
              wx.showToast({
                title: res.data.message,
                icon: 'none',
                mask: true,
                duration: 1500
              })
            }
          },
          fail: function(err) {}, //请求失败
          complete: function(suc) { //请求完成后执行的函数

          }
        })
      }
    })
  },
  //更多信息表单提交
  gengdMsgSubmit: function(e) {
    var that = this;
    var datas = '';
    var qq = e.detail.value.qq;
    // var phone = e.detail.value.phone;
    var fax = e.detail.value.fax;
    var url = e.detail.value.url;
    var wechat = e.detail.value.wechat;
    var microblog = e.detail.value.microblog;
    var introduce = e.detail.value.introduce;
    var industry = e.detail.value.industry;
    var cid = this.data.card.id;
    var ziyuanArr = that.data.ziyuanArr;
    var needArr = that.data.needArr;
    var keyword1;
    var keyword2;
    if (ziyuanArr.length == 2) {
      keyword1 = ziyuanArr[0] + '|' + ziyuanArr[1];
    } else {
      keyword1 = ziyuanArr[0];
    }
    if (needArr.length == 2) {
      keyword2 = needArr[0] + '|' + needArr[1];
    } else {
      keyword2 = needArr[0];
    }
    var my_offer = e.detail.value.my_off;
    var my_need = e.detail.value.my_need;

    if (my_offer.length > 100) {
      wx.showToast({
        title: '我的资源最多100个字符',
        icon: 'none',
        mask: true,
        duration: 900
      })
      return false
    }
    if (my_need.length > 100) {
      wx.showToast({
        title: '我的需求最多100个字符',
        icon: 'none',
        mask: true,
        duration: 900
      })
      return false
    }
    if (qq.length > 20) {
      wx.showToast({
        title: 'QQ最多20个字符',
        icon: 'none',
        mask: true,
        duration: 900
      })
      return false
    }
    if (fax.length > 20) {
      wx.showToast({
        title: '传真最多20个字符',
        icon: 'none',
        mask: true,
        duration: 900
      })
      return false
    }
    if (url.length > 20) {
      wx.showToast({
        title: '网址最多20个字符',
        icon: 'none',
        mask: true,
        duration: 900
      })
      return false
    }
    if (wechat.length > 20) {
      wx.showToast({
        title: '微信最多20个字符',
        icon: 'none',
        mask: true,
        duration: 900
      })
      return false
    }
    if (microblog.length > 20) {
      wx.showToast({
        title: '微博最多20个字符',
        icon: 'none',
        mask: true,
        duration: 900
      })
      return false
    }
    if (introduce.length > 1000) {
      wx.showToast({
        title: '介绍最多1000个字符',
        icon: 'none',
        mask: true,
        duration: 900
      })
      return false
    }
    app.getUserInfo(function(personInfo) {
      var token = personInfo.token;
      var uid = personInfo.uid;
      datas = {
        'qq': qq,
        'industry': industry,
        'fax': fax,
        'url': url,
        'wechat': wechat,
        'microblog': microblog,
        'introduce': introduce,
        'my_offer': my_offer,
        'my_need': my_need,
        'keyword1': keyword1,
        'keyword2': keyword2
      }
      wx.request({
        url: port.CardUpdate, //请求地址
        header: { //请求头
          "Content-Type": "applciation/json",
          "Authorization": "Bearer " + token
        },
        data: {
          data: datas,
          'uid': uid,
          'cid': cid,
          'type': 2
        },
        method: "post", //get为默认方法/POST
        success: function(res) {
          if (res.data.status == 'success') {
            wx.showToast({
              title: '保存成功',
              icon: 'success',
              mask: true,
              duration: 1500,
              success: function() {
                var messageIndex = that.data.messageIndex;
                setTimeout(function() {
                  wx.reLaunch({
                    url: messageIndex + "/myCard?id=" + res.data.data,
                    success: function() {},
                    fail: function() {},
                    complete: function() {}
                  })
                }, 1000)
              }
            })
          }
        },
        fail: function(err) {}, //请求失败
        complete: function(suc) { //请求完成后执行的函数
        }
      })
    })
  },
  //图片模块表单提交
  imageSubmit: function(e) {
    var that = this;
    var title = e.detail.value.title; //标题
    var wheCross = this.data.wheCross;
    var aid = that.data.aid;
    if (title == '') {
      wx.showToast({
        title: '请填写相册标题',
        icon: 'none',
        mask: true,
        duration: 1500
      })
    } else if (title.length > 10) {
      wx.showToast({
        title: '相册标题最多10个字符',
        icon: 'none',
        mask: true,
        duration: 1500
      })
    } else {
      that.setData({
        t: true
      })
      app.getUserInfo(function(personInfo) {
        //wheCross ok：修改相册 no：创建相册
        if (wheCross == "no") {
          wx.request({
            url: port.CreateAlbum, //仅为示例，并非真实的接口地址
            method: 'post',
            data: {
              title: title,
              cid: that.data.cid
            },
            header: {
              'content-type': 'application/json', // 默认值
              "Authorization": "Bearer " + personInfo.token
            },
            success: function(res) {
              if (res.data.status == 'success') {
                if (that.data.tempFilePaths.length > 0) {
                  that.uploadimg({
                    url: port.upload, //这里是你图片上传的接口
                    path: that.data.tempFilePaths, //这里是选取的图片的地址数组
                    aid: res.data.data.id,
                    mold: 2
                  });
                } else {
                  wx.showToast({
                    title: '创建成功',
                    icon: 'success',
                    mask: true,
                    duration: 2000
                  })
                  setTimeout(function() {
                    wx.navigateBack({
                      delta: 1
                    })
                  }, 2000)
                }
              } else {
                that.setData({
                  t: false
                })
                wx.showToast({
                  title: res.data.message,
                  image: '../../imgs/error.png',
                  mask: true,
                  duration: 2000,
                  success: function(r) {}
                })
              }
            }
          })
        } else if (wheCross == "ok") {
          wx.request({
            url: port.CreateAlbum, //仅为示例，并非真实的接口地址
            method: 'POST',
            data: {
              aid: aid,
              title: title,
              cid: that.data.cid
            },
            header: {
              'content-type': 'application/json', // 默认值
              "Authorization": "Bearer " + personInfo.token
            },
            success: function(res) {
              if (res.data.status == 'success') {
                if (that.data.aid) {
                  var arry = that.data.tempFilePaths
                  // that.uploadimg({
                  //   url: port.upload,//这里是你图片上传的接口
                  //   path: arry,//这里是选取的图片的地址数组
                  //   aid: that.data.aid
                  // });
                  wx.showToast({
                    title: res.data.data,
                    icon: 'success',
                    mask: true,
                    duration: 1500,
                  })
                  setTimeout(function() {
                    wx.navigateBack({
                      delta: 1
                    })
                  }, 2000)
                }
              } else {
                wx.showToast({
                  title: res.data.message,
                  image: '../../imgs/error.png',
                  mask: true,
                  duration: 2000,
                  success: function(r) {}
                })
              }
            }
          })
        }
      })
    }
  },
  //视频模块表单提交
  videoSubmit: function(e) {
    var that = this;
    var tempFilePath = that.data.src
    var title = e.detail.value.title
    var cid = that.data.card.id;
    app.getUserInfo(function(personInfo) {
      var uid = personInfo.uid;
      var token = personInfo.token
      var timestamp = Date.parse(new Date()) + ".mp4";
      if (title == "") {
        wx.showToast({
          title: '请填写标题',
          icon: 'none',
          duration: 2000
        })
      } else {
        // if (that.data.video1) {
        //   var datas = {
        //     title: title,
        //     video: that.data.video1,
        //   }
        //   wx.request({
        //     url: port.CardUpdate,//请求地址
        //     header: {//请求头
        //       "Content-Type": "applciation/json",
        //       "Authorization": "Bearer " + token
        //     },
        //     data: { data: datas, 'uid': uid, 'cid': cid, 'type': 4 },
        //     method: "post",//get为默认方法/POST
        //     success: function (res) {
        //       if (res.data.status == 'success') {
        //         wx.hideLoading()
        //         wx.showToast({
        //           title: '保存成功',
        //           icon: 'success',
        //           duration: 2000
        //         })
        //       }
        //     },
        //     fail: function (err) { },//请求失败
        //     complete: function (suc) {//请求完成后执行的函数
        //       // setTimeout(function () {
        //       //   wx.switchTab({
        //       //     url: '../../pages/myCenter/myCenter',
        //       //   })
        //       // }, 2000)
        //     }
        //   })
        // } else {
        if (tempFilePath) {
          wx.showLoading({
            title: '上传中...',
          })
          that.setData({
            zhezhaoc: true
          })
          wx.uploadFile({
            url: port.uploadVideo,
            filePath: tempFilePath,
            name: 'file',
            formData: {
              name: timestamp,
              uid: uid
            },
            success: function(ress) {
              var data = JSON.parse(ress.data)
              var datas = {
                title: title,
                video: data.data,
              }
              wx.request({
                url: port.CardUpdate, //请求地址
                header: { //请求头
                  "Content-Type": "applciation/json",
                  "Authorization": "Bearer " + token
                },
                data: {
                  data: datas,
                  'uid': uid,
                  'cid': cid,
                  'type': 4
                },
                method: "post", //get为默认方法/POST
                success: function(res) {
                  if (res.data.status == 'success') {
                    wx.hideLoading()
                    wx.showToast({
                      title: '上传成功',
                      icon: 'success',
                      duration: 2000
                    })
                    that.setData({
                      zhezhaoc: false
                    })
                    var messageIndex = that.data.messageIndex;
                    wx.switchTab({
                      url: messageIndex + '/myCard',
                    })
                    // setTimeout(function () {
                    //   wx.reLaunch({
                    //     url: "../../pages/myCenter/myCenter?id=" + res.data.data,
                    //     success: function () { },
                    //     fail: function () { },
                    //     complete: function () { }
                    //   })
                    // }, 2000)
                  }
                },
                fail: function(err) {}, //请求失败
                complete: function(suc) { //请求完成后执行的函数

                }
              })
            },
            faill: function(res) {}
          })
        } else {
          wx.showToast({
            title: '请上传视频',
            icon: 'none',
            duration: 2000
          })
        }
        // }
      }
    })
  },
  //选择手机或者微信手机号
  pickerPhone: function(e) {
    var that = this;
    if (e.detail.value == 0) {
      that.setData({
        ty: true,
        codeShow: false
      })
    } else if (e.detail.value == 1) {
      that.setData({
        ty: false,
      })
      if (that.data.cid == 0) {
        that.setData({
          codeShow: true
        })
      }
    }

  },

  back: function() {
    var that = this;
    wx.navigateBack({
      delta: 1
    })
  },

  backCenter: function() {
    var that = this;
    var messageIndex = that.data.messageIndex;
    wx.switchTab({
      url: messageIndex + '/myCard',
    })
  },

  //地址选择
  bindRegionChange: function(e) {
    this.setData({
      region: e.detail.value
    })
  },
  //这里是选取图片的方法
  choose: function() {
    var that = this;

    wx.chooseImage({
      count: 1,
      success: function(res) {
        var tempFilePaths = res.tempFilePaths
        that.setData({
          tximg: tempFilePaths[0],
          src: tempFilePaths[0],
          avatar: tempFilePaths[0],
        })
      },
      faill: function(res) {}
    })
  },

  //修改视频
  // changeNewVideo: function () {
  //   var that = this;
  //   that.deleteVideo();
  // },
  //添加视频
  addVideo: function() {
    var that = this
    wx.chooseVideo({
      sourceType: ['album', 'camera'],
      maxDuration: 60,
      camera: 'back',
      success: function(res) {
        if (res.size / (1024 * 1024) < 50) {
          that.setData({
            src: res.tempFilePath,
            hidden: false,
            video: res.tempFilePath,
            vIfShow: false,
            videoPlay: "block"
          })
        } else {
          wx.showToast({
            icon: 'success',
            image: '/images/error.png',
            title: '请上传小于50MB',
          })
          return;
        }

      }
    })
    var tempFilePath = that.data.src
    var title = that.data.title
    var cid = that.data.card.id;
    app.getUserInfo(function(personInfo) {
      var uid = personInfo.uid;
      var token = personInfo.token
      var timestamp = Date.parse(new Date()) + ".mp4";
      if (tempFilePath) {
        wx.showLoading({
          title: '上传中...',
        })
        wx.uploadFile({
          url: port.uploadVideo,
          filePath: tempFilePath,
          name: 'file',
          formData: {
            name: timestamp,
            uid: uid
          },
          success: function(ress) {
            var data = JSON.parse(ress.data)
            var datas = {
              title: title,
              video: data.data,
            }
            wx.request({
              url: port.CardUpdate, //请求地址
              header: { //请求头
                "Content-Type": "applciation/json",
                "Authorization": "Bearer " + token
              },
              data: {
                data: datas,
                'uid': uid,
                'cid': cid,
                'type': 4
              },
              method: "post", //get为默认方法/POST
              success: function(res) {
                if (res.data.status == 'success') {
                  wx.hideLoading()
                  wx.showToast({
                    title: '上传成功',
                    icon: 'success',
                    duration: 2000
                  })
                  var messageIndex = that.data.messageIndex;
                  wx.switchTab({
                    url: messageIndex + '/myCard',
                  })
                }
              },
              fail: function(err) {}, //请求失败
              complete: function(suc) { //请求完成后执行的函数
              }
            })
          },
          faill: function(res) {}
        })
      } else {
        wx.showToast({
          title: '请上传视频',
          icon: 'none',
          duration: 2000
        })
      }


    })
  },
  //删除视频
  deleteVideo: function() {
    var that = this;
    app.getUserInfo(function(personInfo) {
      wx.request({
        url: port.DelVideo, //仅为示例，并非真实的接口地址
        method: 'get',
        data: {
          cid: that.data.card.id
        },
        header: {
          'content-type': 'application/json', // 默认值
          "Authorization": "Bearer " + personInfo.token
        },
        success: function(res) {
          if (res.data.status == 'success') {
            wx.showToast({
              title: '删除成功',
              icon: 'success',
              duration: 2000
            })
            that.setData({
              video: "",
              vIfShow: true,
              videoPlay: 'none',
              title: ''
            })
            var messageIndex = that.data.messageIndex;
            wx.switchTab({
              url: messageIndex + '/myCard',
            })
          } else {
            wx.showToast({
              title: res.data.message,
              icon: 'error',
              duration: 2000
            })
          }
        }
      })

    })
  },

  //获取当前位置
  getLocation: function() {
    var that = this;
    wx.chooseLocation({
      success: function(res) {
        var card = that.data.card;
        card.address = res.name;
        that.setData({
          card: card
        })
      },
    })
  },

  // author:zhoujing
  // 我的资源
  inputKey: function(e) {
    this.setData({
      key: e.detail.value
    })
  },
  submitKey: function() {

    if (this.data.ziyuanArr.length >= 2) {
      wx.showToast({
        title: '添加两个关键词就可以了哦',
        icon: 'none',
        mask: true,
        duration: 1500
      })
      return false
    } else {
      var that = this;
      if (that.data.key == null) {
        wx.showToast({
          title: '请输入关键词',
          icon: 'none',
          mask: true,
          duration: 1500
        })
      } else {
        var ziyuanArr = new Array();
        ziyuanArr = that.data.ziyuanArr;
        ziyuanArr.push(that.data.key)
        that.setData({
          ziyuanArr: ziyuanArr,
          myziyuan: ''
        })
      }

    }

  },
  delKey: function(e) {
    var that = this;
    var ziyuanArr = new Array();
    ziyuanArr = that.data.ziyuanArr;
    ziyuanArr.splice(e.currentTarget.id, 1)
    that.setData({
      ziyuanArr: ziyuanArr
    })

  },
  // 我的资源结束
  // 我的需求
  inputNeedKey: function(e) {
    this.setData({
      key: e.detail.value
    })
  },
  needKey: function() {
    if (this.data.needArr.length >= 2) {
      wx.showToast({
        title: '添加两个关键词就可以了哦',
        icon: 'none',
        mask: true,
        duration: 1500
      })
      return false;
    } else {
      var that = this;
      if (that.data.key == null) {
        wx.showToast({
          title: '请输入关键词',
          icon: 'none',
          mask: true,
          duration: 1500
        })
      } else {
        var needArr = that.data.needArr;
        needArr.push(that.data.key)
        that.setData({
          needArr: needArr,
          myxuqiu: ''
        })
      }
    }

  },
  delNeedKey: function(e) {
    var that = this;
    var needArr = new Array();
    needArr = that.data.needArr;
    needArr.splice(e.currentTarget.id, 1)
    that.setData({
      needArr: needArr
    })

  },
  // 我的需求结束
  // author:zhoujing结束

  //选择机构
  bindMultiPickerChange: function(option) {
    var that = this;
    var value_id = option.detail.value;
    var objectMultiArray = new Array();
    objectMultiArray = that.data.objectMultiArray;
    for (var i = 0; i < objectMultiArray.length; i++) {
      if (objectMultiArray[i].id == value_id) {
        var value = objectMultiArray[i].name;
        that.setData({
          institution: value,
          institutioncolor: 'black'
        })
      }
    }
  },

  //获取用户信息
  getuserinfo: function(e) {
    var that = this;
    var result = e.detail.errMsg;
    if (result == 'getUserInfo:ok') {
      var tximg = e.detail.userInfo.avatarUrl;
      that.setData({
        tximg: tximg,
        avatar: tximg
      })
    }
    app.getUserInfo(function(personInfo) {
      var token = personInfo.token;
      var uid = personInfo.uid;
      var userinfo = new Array();
      userinfo = e.detail.userInfo;
      wx.request({
        url: port.setuserinfo, //请求地址
        header: { //请求头
          "Content-Type": "applciation/json",
          "Authorization": "Bearer " + token
        },
        data: {
          'userinfo': userinfo,
          'uid': uid
        },
        method: "post", //get为默认方法/POST
        success: function(res) {
          if (res.data.status == 'success') {

          } else {
            that.setData({
              t: true
            })
            wx.showToast({
              title: res.data.message,
              icon: 'none',
              mask: true,
              duration: 1500
            })
          }
        },
        fail: function(err) {}, //请求失败
        complete: function(suc) { //请求完成后执行的函数

        }
      })
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function(res) {
    this.videoContext = wx.createVideoContext("video1");
  },
  inputValue: '',
  bindInputBlur: function(e) {
    this.inputValue = e.detail.value
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function(options) {},

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})