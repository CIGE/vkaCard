var app = getApp();
var QQMapWX = require("../../qqmap-wx-jssdk.min.js")
var demo = new QQMapWX({
  key: 'UTFBZ-RBN3V-FNFPU-U4XES-2RK62-TDFPK' // 必填
});
const port = require('../../config.js');
const mainIndex = require('../index.js');

Page({
  data: {
    applyNum: '',
    hasApp: '',
    nonet: true,//网络状态
    adress: "",
    hidden: true,//控制两个页面切换
    objectMultiArray: [
      {
        id: 0,
        name: '商务之风',
        url: port.imgPath + "new-background/newbg-2.png",
      },
      {
        id: 1,
        name: '浩瀚银河',
        url: port.imgPath + "new-background/newbg-3.png",
      },
      {
        id: 2,
        name: '黑色旋律',
        url: port.imgPath + "new-background/newbg-4.png",
      },
      {
        id: 3,
        name: '鱼池徜徉',
        url: port.imgPath + "new-background/newbg-1.png",
      },
      {
        id: 4,
        name: '笔墨山水',
        url: port.imgPath + "new-background/newbg-5.png",
      },
    ],
    color: '#000',
    bgImg: '',
    className: false,
    card: [],
    imgPath: port.imgPath,
    video: '',
    title: '',
    colorid: '',
    imagePath: "",
    imageEwm: '', //二维码
    maskHidden: true,
    imgModel: '',
    limit: 1,
    tatol: 0,
    nonet: true,
    checkLoaded: 'none',
    checkLoadding: 'flex',
    bgImgColor: 'white',
    bigBg: '',
    id_smallbg: '',
    phoneImg: '',
    emailImg: '',
    adressImg: '',
    seeImg: '',
    iconColor: '',
    sindex: '',
    cardCheck: 1,   //名片首页智能引导
    prompt: 4,
    createPrompt: 0,
    indexShuttle: mainIndex.indexShuttle,  //跳转分包shuttle统一路径
    indexMessage: mainIndex.indexMessage,  //跳转分包message统一路径
    indexCenter: mainIndex.indexCenter, //跳转分包center统一路径
    broadcastNum: 'turn1',  //广播列
    zShareIndex: 0,  //分享层级
    zPerIndex: 0,   //完善信息层级
    zApplyIndex: 0,  //好友申请层级
    broadcast: 1,  //是否显示广播
    broadcastlist: [],  //广播所有消息
    casting: [],  //广播弹窗
    epitomelist: [],  //广播条列
    createScanzIndex: 0   //拍名片创建名片层级
  },

  onLoad: function (option) {
    var that = this;
    wx: wx.getSystemInfo({
      success: function (res) {
        var screenHeight = res.screenHeight;
        that.setData({
          screenHeight: screenHeight
        })

      },

      fail: function (res) { },
      complete: function (res) { },
    })
    wx.hideShareMenu();
    var that = this;
    wx.showLoading({
      title: '加载中',
    })
    app.NetworkState();
    // 页面初始化 options为页面跳转所带来的参数
    var size = this.setCanvasSize();//动态设置画布大小
    app.getUserInfo(function (personInfo) {
      var token = personInfo.token;
      var uid = personInfo.uid;
      wx.request({
        url: port.CardMy,//请求地址
        header: {//请求头
          "Content-Type": "applciation/json",
          "Authorization": "Bearer " + token
        },
        data: { 'uid': uid },
        method: "post",//get为默认方法/POST
        success: function (res) {
          if (res.data.status == 'success') {
            setTimeout(function () {
              that.setData({
                checkLoadding: 'none',
                checkLoaded: 'block'
              })
            }, 1000)
            if (res.data.data.video != '' && res.data.data.video != null) {
              var video = JSON.parse(res.data.data.video);
              that.setData({
                title: video.title,
                video: video.video,
              })
            }
            var colorid = res.data.data.color.colorid > 0 ? res.data.data.color.colorid : 1;
            var color = res.data.data.color.color != '' && res.data.data.color.color != null ? res.data.data.color.color : '#000';
            var background = res.data.data.templet_sz != "" && res.data.data.templet_sz != null ? res.data.data.templet_sz.smallBg : port.imgPath + "/new-background/newbg-2.png";
            //根据id_smallbg值定义图标
            var id_smallbg = res.data.data.templet_sz.id_smallbg;
            that.setData({
              sindex: id_smallbg,
            })
            if (id_smallbg == 0) var iconColor = 'white'
            else if (id_smallbg == 1) var iconColor = 'white'
            else if (id_smallbg == 2) var iconColor = 'gold'
            else if (id_smallbg == 3) var iconColor = 'gray'
            else if (id_smallbg == 4) var iconColor = 'black'
            // var iconColor = res.data.data.templet_sz != "" && res.data.data.templet_sz != null ? res.data.data.templet_sz.iconColor: "white";
            for (var key in res.data.data) {
              if (res.data.data[key] == null) {
                res.data.data[key] = ""
              }
            }
            that.setData({
              bgImg: background,
              color: color,
              phoneImg: '/images/ic_phone_' + iconColor + '@3x.png',
              emailImg: '/images/ic_email_' + iconColor + '@3x.png',
              adressImg: '/images/ic_address_' + iconColor + '@3x.png',
              seeImg: '/images/ic_preview_' + iconColor + '@3x.png',
              card: res.data.data,
              user_imgs: res.data.data.imgs,
              hidden: true,
              adress: res.data.data.ssq + res.data.data.address,
              tatol: res.data.tatol,
              bgImgColor: res.data.data.templet_sz.bgImgColor,
            })
            wx.hideLoading()
          } else {
            that.setData({
              hidden: false,
              prompt: 4,
              cardCheck: 0,
              createScanzIndex: 99,
            })
            wx.hideLoading()
          }
        },
        fail: function (err) {
          wx.hideLoading()
        },//请求失败
        complete: function (suc) {//请求完成后执行的函数
        }
      })

      //获取申请的人数
      var openid = personInfo.openId;
      wx.request({
        url: port.applylist,//请求地址
        header: {//请求头
          "Content-Type": "applciation/json",
          "Authorization": "Bearer " + token
        },
        data: { 'openid': openid },
        method: "post",//get为默认方法/POST
        success: function (res) {
          var applyNum = 0;
          if(res.data.data.length > 0){
            for(var i = 0;i < res.data.data.length;i++){
              if(res.data.data[i].status == 0){
                applyNum++;
              }
            }
          }
          var hasApp;
          if (applyNum == 0) {
            hasApp = false;
          } else {
            hasApp = true;
          }
          that.setData({
            applyNum: applyNum,
            hasApp: hasApp,
          })
        },
        fail: function (err) {
        },
        complete: function (suc) {
        }
      })

      //判断是否首次进入(智能引导)
      var cardCheck = wx.getStorage({
        key: 'cardCheck',
        success: function (res) {
          if (res.data == 0) {
            that.setData({
              cardCheck: 0,
              prompt: 1,
              zShareIndex: 99
            })
          } else if (res.data == 1) {
            that.setData({
              cardCheck: 1,
            })
          }
        },
      })
      that.broadcastMsg(token, uid, function (result) {
        if (result == true) {
          //判断是否首次进入(广播)
          var broadcast = wx.getStorage({
            key: 'broadcast',
            success: function (res) {
              if (res.data == 0) {
                that.setData({
                  broadcast: 0,
                  cardCheck: 1,
                  zShareIndex: 0
                })
              } else if (res.data == 1) {
                that.setData({
                  broadcast: 1,
                })
              }
            },
          })
        }
      });

    })
    setInterval(function () {
      var castNum = that.data.broadcastNum;
      castNum = castNum.substring(4);
      if (castNum < that.data.broadcastlist.length) {
        castNum++;
      } else {
        castNum = 1;
      }
      var broadcastNum = 'turn' + castNum;
      that.setData({
        broadcastNum: broadcastNum,
      })
    }, 6000)
  },

  //广播关闭
  closeCast: function () {
    var that = this;
    wx.setStorage({
      key: 'broadcast',
      data: 1,
    })
    var broadcast = wx.getStorage({
      key: 'broadcast',
      success: function (res) {
        if (res.data == 0) {
          that.setData({
            broadcast: 0,
          })
        } else if (res.data == 1) {
          that.setData({
            broadcast: 1,
            cardCheck: 0,
            prompt: 1,
            zShareIndex: 99
          })
        }
      },
    })
  },
  //跳转会员中心
  vipCenter: function () {
    var that = this;
    var cid = that.data.card.id;
    if (!cid) {
      wx.showToast({
        icon: 'success',
        image: '/images/error.png',
        title: '请先添加名片',
      })
    } else {
      var indexShuttle = that.data.indexShuttle;
      wx.navigateTo({
        url: indexShuttle + '/vipCenter/vipCenter',
      })
    }
    that.closeCast();
  },
  //智能引导
  nextPrompt: function (e) {
    var that = this;
    var prompt = e.currentTarget.dataset.prompt;
    var createPrompt = 0;
    if (prompt == 1) {
      prompt = 2;
      that.setData({
        zShareIndex: 0,
        zPerIndex: 99,
      })
    } else if (prompt == 2) {
      prompt = 3;
      that.setData({
        zPerIndex: 0,
        zApplyIndex: 99,
      })
    } else if (prompt == 3) {
      wx.setStorage({
        key: 'cardCheck',
        data: 1,
      })
    }
    if (prompt == 4) {
      createPrompt = 1;
      that.setData({
        createScanzIndex: 0
      })
    }
    that.setData({
      prompt: prompt,
      createPrompt: createPrompt
    })
    var cardCheck = wx.getStorage({
      key: 'cardCheck',
      success: function (res) {
        if (res.data == 0) {
          that.setData({
            cardCheck: 0,
          })
        } else if (res.data == 1) {
          that.setData({
            cardCheck: 1,
            zApplyIndex: 0
          })
        }
      },
    })
  },

  //广播消息获取
  broadcastMsg: function (token, uid, cb) {
    var that = this;
    var token = token;
    var uid = uid;
    wx.request({
      url: port.broadcastMessage,//请求地址
      header: {//请求头
        "Content-Type": "applciation/json",
        "Authorization": "Bearer " + token
      },
      data: { 'uid': uid },
      method: "post",//get为默认方法/POST
      success: function (res) {
        if (res.data.status == 'success') {
          var casting = new Array();
          var epitomelist = new Array();
          for (var i = 0; i < res.data.data.length; i++) {
            var epitomeObject = new Object();
            epitomeObject.epitome = res.data.data[i].epitome;
            epitomeObject.id = i + 1;
            epitomelist.push(epitomeObject);
            that.setData({
              epitomelist: epitomelist
            })
          }
          for (var i = 0; i < res.data.data.length; i++) {
            if (res.data.data[i].is_main == 1) {
              var castObject = new Object();
              castObject.content = res.data.data[i].content;
              castObject.vicetitle = res.data.data[i].subtitle;
              castObject.maintitle = res.data.data[i].title;
              casting.push(castObject);
              that.setData({
                casting: casting
              })
            }
          }
          that.setData({
            broadcastlist: res.data.data
          })
          return typeof cb == "function" && cb(true)
        } else {
          return typeof cb == "function" && cb(false)
        }
      },
      fail: function (err) {
      },//请求失败
      complete: function (suc) {//请求完成后执行的函数
      }
    })
  },

  onReachBottom: function () {
    var that = this;
    var img = that.data.user_imgs;
    var cid = that.data.card.id;
    var limit = that.data.limit;
    var tatol = that.data.tatol
    if (img.length < tatol) {
      app.getUserInfo(function (personInfo) {
        var token = personInfo.token;
        wx.request({
          url: port.CardMyImg,//请求地址
          header: {//请求头
            "Content-Type": "applciation/json",
            "Authorization": "Bearer " + token
          },
          data: { 'cid': cid, limit: limit },
          method: "post",//get为默认方法/POST
          success: function (res) {
            if (res.data.status == 'success') {
              for (var i = 0; i < res.data.data.length; i++) {
                img.push(res.data.data[i])
              }
              that.setData({
                user_imgs: img,
                limit: that.data.limit + 1,
                tatol: res.data.tatol
              })
            }
          }
        })
      })
    }
  },

  onShow: function () {
    var that = this;
    that.onLoad();

  },

  //点击弹出广播栏
  showCast: function () {

  },

  //扫描名片
  scanCard() {
    var that = this;
    wx.chooseImage({
      count: 1, // 默认9  
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有  
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有  
      success: function (res) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片  
        that.setData({
          src: res.tempFilePaths[0],
        })
        //提示
        wx.showToast({
          title: '正在识别信息',
          icon: 'loading'
        })
        app.getUserInfo(function (personInfo) {
          var token = personInfo.token;
          var openid = personInfo.openId;
          //请求token
          wx.request({
            url: port.CardOCRAuth,//请求地址
            header: {//请求头
              "Content-Type": "applciation/json",
              "Authorization": "Bearer " + token
            },
            data: { openid: openid },
            method: "post",//get为默认方法/POST
            success: function (res) {
              // 腾讯云ORC请求
              wx.uploadFile({
                url: 'https://recognition.image.myqcloud.com/ocr/businesscard',
                filePath: that.data.src,
                name: 'image',
                header: {//请求头
                  "authorization": res.data.data.signStr,
                },
                formData: {
                  appid: res.data.data.appid,
                },
                method: "post",
                success: function (res) {
                  var scanData = JSON.parse(res.data).result_list[0].data;
                  that.setData({
                    scanData: scanData,
                  })
                  if (scanData.length == 0) {
                    wx.showToast({
                      title: '扫描失败',
                      icon: 'none',
                    })
                  } else {
                    var dataStr = '?';
                    for (var i = 0; i < scanData.length; i++) {
                      dataStr += scanData[i].item + '=' + scanData[i].value + '&';
                    }
                    dataStr = dataStr.substring(0, dataStr.length - 1);
                    var indexMessage = that.data.indexMessage;
                    wx.navigateTo({
                      url: indexMessage + '/newpermessage/newpermessage' + dataStr,
                    })
                  }
                },
                fail: function (err) { },
                complete: function (suc) { },
              })
            },
            fail: function (err) { },//请求失败
            complete: function (suc) {//请求完成后执行的函数
            }
          })
        })
      }
    })
  },

  // 头像放大
  headerImg: function () {
    var that = this;
    var imgs = that.data.card.avatar
    wx.previewImage({
      urls: [that.data.imgPath + imgs], // 需要预览的图片http链接列表
    })
  },
  //图片点击放大
  imgYu: function (e) {
    var that = this;
    var imgs = that.data.user_imgs
    var arr = [];
    for (var i = 0; i < imgs.length; i++) {
      var list = that.data.imgPath + imgs[i]
      arr.push(list)
    }
    var current = e.target.dataset.src;
    wx.previewImage({
      current: current, // 当前显示图片的http链接
      urls: arr, // 需要预览的图片http链接列表
    })
  },
  makeCards: function () {
    var that = this;
    var indexMessage = that.data.indexMessage;
    wx.navigateTo({
      url: indexMessage + '/newpermessage/newpermessage',
    })
  },

  //预览
  goYulan: function () {
    var that = this;
    var indexShuttle = that.data.indexShuttle;
    wx.navigateTo({
      url: '/pages/index/preview?cid=' + this.data.card.id + '&my=1',
    })
  },
  //名片码
  openCode: function () {
    wx.showLoading({
      title: '生成中...',
    })
    var that = this;
    app.getUserInfo(function (personInfo) {
      var token = personInfo.token;
      var uid = personInfo.uid;
      wx.request({
        url: port.Code,//请求地址
        header: {//请求头
          "Content-Type": "applciation/json",
          "Authorization": "Bearer " + token
        },
        data: { 'uid': uid, 'cid': that.data.card.id, 'avatar': that.data.imgPath + that.data.card.avatar },
        method: "post",//get为默认方法/POST
        success: function (res) {
          if (res.data.status == 'success') {
            wx.downloadFile({
              url: port.code + res.data.data, //仅为示例，并非真实的资源
              success: function (ress) {
                if (ress.statusCode === 200) {
                  that.setData({
                    imageEwm: ress.tempFilePath,
                  })
                  that.createNewImg();
                }
              }
            })
          } else {
            wx.showToast({
              title: res.data.message,
              icon: 'none',
              mask: true,
              duration: 2000,
              success: function (e) {
                setTimeout(function () {
                  wx.hideLoading()
                }, 2000)
              }
            })
          }
        }
      })
    })
  },
  // 小程序二维码
  //适配不同屏幕大小的canvas    生成的分享图宽高分别是 750  和940，老实讲不知道这块到底需不需要，然而。。还是放了，因为不写这块的话，模拟器上的图片大小是不对的。。。
  setCanvasSize: function () {
    var size = {};
    try {
      var res = wx.getSystemInfoSync();
      var scale = 450;//画布宽度
      var scaleH = 1200 / 750;//生成图片的宽高比例
      var width = res.windowWidth;//画布宽度
      var height = this.data.screenHeight;//画布的高度
      size.w = width;
      size.h = height;
    } catch (e) {
      // Do something when catch error
    }
    return size;
  },
  //将名字绘制到canvas的固定
  settextFir: function (context) {
    let that = this;
    var size = that.setCanvasSize();
    var textFir = that.data.card.username;
    context.setFontSize(25);
    context.fillText(textFir, 20, size.h * 0.08, 300);
    // context.fillText(textFir, size.w / 17, size.h * 0.4);    
    context.stroke();
  },
  //将职业绘制到canvas的固定
  settextWork: function (context) {
    let that = this;
    var size = that.setCanvasSize();
    var textSec = that.data.card.duty;
    context.setFontSize(23);
    // context.fillText(textSec, size.w /17, size.h * 0.463);
    context.fillText(textSec, 20, size.h * 0.13, 300);
    context.stroke();
  },
  //将公司绘制到canvas的固定
  settextCompany: function (context) {
    let that = this;
    var size = that.setCanvasSize();
    var textSec = that.data.card.company;
    context.setFontSize(15);
    context.fillText(textSec, 20, size.h * 0.25, 300);
    // context.fillText(textSec, size.w / 17, size.h * 0.53);
    context.stroke();
  },
  //将电话绘制到canvas的固定
  settextPhone: function (context) {
    let that = this;
    var size = that.setCanvasSize();
    var textSec = that.data.card.mobile;
    context.setFontSize(14.1);
    context.fillText(textSec, 42, size.h * 0.29, 300);
    // context.fillText(textSec, size.w / 9, size.h * 0.58);
    context.stroke();
  },
  //将详细地址绘制到canvas的固定
  settextPlace: function (context) {
    let that = this;
    // var size = that.setCanvasSize();
    let textSec1 = that.data.card.ssq;
    let textSec2 = that.data.card.address;
    let textSec = `${textSec1}-${textSec2}`;
    // context.setFontSize(15);
    // context.fillText(textSec, size.w / 2.3, size.h * 0.632);
    // context.stroke();
    var text = textSec//这是要绘制的文本
    var chr = text.split("");//这个方法是将一个字符串分割成字符串数组
    var temp = "";
    var row = [];
    context.setFontSize(14.1)
    for (var a = 0; a < chr.length; a++) {
      if (context.measureText(temp).width < 250) {
        temp += chr[a];
      }
      else {
        a--; //这里添加了a-- 是为了防止字符丢失，效果图中有对比
        row.push(temp);
        temp = "";
      }
    }
    row.push(temp);

    //如果数组长度大于2 则截取前两个
    if (row.length > 2) {
      var rowCut = row.slice(0, 2);
      var rowPart = rowCut[1];
      var test = "";
      var empty = [];
      for (var a = 0; a < rowPart.length; a++) {
        if (context.measureText(test).width < 220) {
          test += rowPart[a];
        }
        else {
          break;
        }
      }
      empty.push(test);
      var group = empty[0] + "..."//这里只显示两行，超出的用...表示
      rowCut.splice(1, 1, group);
      row = rowCut;
    }
    for (var b = 0; b < row.length; b++) {
      context.fillText(row[b], 42, b * 18 + that.data.screenHeight / 3.07, 300);
    }
  },

  //将canvas转换为图片保存到本地，然后将图片路径传给image图片的src
  createNewImg: function () {
    var that = this;
    var size = that.setCanvasSize();
    var context = wx.createCanvasContext('myCanvas');
    var path = "/images/002.png";//背景
    var iphonePath = "/images/iphonePath.png";
    var placePath = "/images/placePath.png";
    var imageEwm = that.data.imageEwm;
    var erweiborder = "/images/erweiborder.png";
    context.setGlobalAlpha = 0;
    context.drawImage(path, 0, 0, size.w / 1.02, size.h / 2.7);
    // context.drawImage(iphonePath, 20, size.h * 0.27, size.w * 0.03, size.h * 0.022);
    // context.drawImage(placePath, 20, size.h * 0.305, size.w * 0.03, size.h * 0.024);
    // context.drawImage(erweiborder, size.w / 2 + 40.5, 14.5, size.w * 0.31, size.w * 0.31);
    context.drawImage(imageEwm, size.w / 4.5, size.w / 12, size.w * 0.52, size.w * 0.5);
    // this.settextFir(context);
    // if (that.data.card.duty != null) {
    //   this.settextWork(context);
    // }
    // this.settextPhone(context);
    // this.settextPlace(context);
    // this.settextCompany(context);
    //绘制图片
    context.draw();
    //将生成好的图片保存到本地，需要延迟一会，绘制期间耗时

    wx.canvasToTempFilePath({
      canvasId: 'myCanvas',
      success: function (res) {
        var tempFilePath = res.tempFilePath;
        that.setData({
          imagePath: tempFilePath,
          canvasHidden: false,
          maskHidden: true,
        });
        //将生成的图片放入到《image》标签里
        var img = that.data.imagePath;
        wx.previewImage({
          current: img, // 当前显示图片的http链接
          urls: [img] // 需要预览的图片http链接列表
        })
        wx.hideLoading()
      },
      fail: function (res) {
      }
    });

  },
  //更换背景
  bindMultiPickerChange2: function (e) {
    var that = this;
    var imgPath = that.data.imgPath + 'new-background/';
    var index = e.currentTarget.dataset.id;
    that.setData({
      sindex: index,
    })
    if (index == 0) {
      that.setData({
        bgImg: imgPath + "newbg-2.png",
        bgImgColor: 'white',
        iconColor: 'white',
        bigBg: imgPath + "newbg-2long.png",
        id_smallbg: index,
        phoneImg: '/images/ic_phone_white@3x.png',
        emailImg: '/images/ic_email_white@3x.png',
        adressImg: '/images/ic_address_white@3x.png',
        seeImg: '/images/ic_preview_white@3x.png',
      })
    }
    if (index == 1) {
      that.setData({
        bgImg: imgPath + 'newbg-3.png',
        bgImgColor: 'white',
        iconColor: 'white',
        bigBg: imgPath + "newbg-3long.png",
        id_smallbg: index,
        phoneImg: '/images/ic_phone_white@3x.png',
        emailImg: '/images/ic_email_white@3x.png',
        adressImg: '/images/ic_address_white@3x.png',
        seeImg: '/images/ic_preview_white@3x.png',
      })

    }
    if (index == 2) {
      that.setData({
        bgImg: imgPath + 'newbg-4.png',
        bgImgColor: '#AE966C',
        iconColor: 'gold',
        bigBg: imgPath + "newbg-4long.png",
        id_smallbg: index,
        phoneImg: '/images/ic_phone_gold@3x.png',
        emailImg: '/images/ic_email_gold@3x.png',
        adressImg: '/images/ic_address_gold@3x.png',
        seeImg: '/images/ic_preview_gold@3x.png',
      })
    }
    if (index == 3) {
      that.setData({
        bgImg: imgPath + 'newbg-1.png',
        bgImgColor: '#666664',
        iconColor: 'gray',
        bigBg: imgPath + "newbg-1long.png",
        id_smallbg: index,
        phoneImg: '/images/ic_phone_gray@3x.png',
        emailImg: '/images/ic_email_gray@3x.png',
        adressImg: '/images/ic_address_gray@3x.png',
        seeImg: '/images/ic_preview_gray@3x.png',
      })
    }
    if (index == 4) {
      that.setData({
        bgImg: imgPath + 'newbg-5.png',
        bgImgColor: 'black',
        iconColor: 'black',
        bigBg: imgPath + "newbg-5long.png",
        id_smallbg: index,
        phoneImg: '/images/ic_phone_black@3x.png',
        emailImg: '/images/ic_email_black@3x.png',
        adressImg: '/images/ic_address_black@3x.png',
        seeImg: '/images/ic_preview_black@3x.png',
      })
    }
    var backgroundUrl = new Object();
    var sz = that.data.card.templet_sz;
    backgroundUrl.smallBg = that.data.bgImg;
    backgroundUrl.bgImgColor = that.data.bgImgColor;
    backgroundUrl.bigBg = that.data.bigBg;
    backgroundUrl.h5Bg = sz.h5Bg;
    backgroundUrl.h5Color = sz.h5Bg;
    backgroundUrl.id_smallbg = that.data.id_smallbg;
    // backgroundUrl.iconColor= that.data.iconColor;
    backgroundUrl.id_h5bg = sz.id_h5bg;
    var datas = '';
    var cid = that.data.card.id

    app.getUserInfo(function (personInfo) {
      var token = personInfo.token;
      var uid = personInfo.uid;
      datas = {
        'color': that.data.color,
        'colorid': index,
        'background': that.data.bgImg,
        'templet_sz': backgroundUrl
      }
      wx.request({
        url: port.CardUpdate,//请求地址
        header: {//请求头
          "Content-Type": "applciation/json",
          "Authorization": "Bearer " + token
        },
        data: { data: datas, 'uid': uid, 'cid': cid, 'type': 6 },
        method: "post",//get为默认方法/POST
        success: function (res) {
          if (res.data.status == 'success') {
            wx.navigateTo({
              url: "myCard?id=" + res.data.data,
            })
          }
        },
        fail: function (err) { },//请求失败
        complete: function (suc) {//请求完成后执行的函数
        }
      })
    })
  },

  wffx: function () {
    wx.showToast({
      title: '此名片涉嫌违规 无法分享',
      icon: 'none',
      mask: true,
      success: function () {
        return false
      }
    })
  },
  onShareAppMessage: function (res) {
    var that = this;
    var indexShuttle = that.data.indexShuttle;
    var path = "/pages/index/preview?cid=" + that.data.card.id + "&binding=" + 1;  //binding=1 分享绑定上级
    // var id_h5Bg = that.data.card.templet_sz.id_h5bg;
    // if (id_h5Bg == 1) {
    //   path = '/pages/animateCard/animateCard?cid=' + this.data.card.id + '&binding=' + 1
    // } else if (id_h5Bg == 2) {
    //   path = '/pages/animateCard/animateCardSecond?cid=' + this.data.card.id + '&binding=' + 1
    // } else if (id_h5Bg == 3) {
    //   path = '/pages/animateCard/animateThird?cid=' + this.data.card.id + '&binding=' + 1
    // } else if (id_h5Bg == "") {
    //   path = '/pages/animateCard/animateThird?cid=' + this.data.card.id + '&binding=' + 1
    // }
    return {
      title: 'V咖片',
      path: path,
      success: function (res) {
      },
      fail: function (res) {
        // 转发失败
      }
    }
  },

  //交換申請入口
  exchange: function () {
    var that = this;
    var indexShuttle = that.data.indexShuttle;
    wx.navigateTo({
      url: indexShuttle + '/exchange/exchange',
    })
  },
  // 点击出现地图
  goMap: function () {
    var that = this

    demo.geocoder({
      address: that.data.adress,
      success: res => {

        var lat = res.result.location.lat;
        var lng = res.result.location.lng
        wx.getLocation({
          type: 'gcj02', //返回可以用于wx.openLocation的经纬度
          success: function (res) {
            wx.openLocation({
              latitude: lat,
              longitude: lng,
              scale: 28,
              name: that.data.adress
            })
          }
        })
      },
      fail: function (res) {
      },
      complete: function (res) {
      }
    });
  },
  //個人信息
  permessage: function () {
    var that = this;
    var cid = that.data.card.id;
    if (!cid) {
      wx.showToast({
        icon: 'success',
        image: '/images/error.png',
        title: '请先添加名片',
      })
    } else {
      var indexMessage = that.data.indexMessage;
      wx.navigateTo({
        url: indexMessage + '/permessage/permessage?cid=' + cid,
      })
    }
  },
  broadcastScroll: function () {
    return false;
  }
})