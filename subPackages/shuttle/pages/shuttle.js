/**
 * 分包shuttle配置文件
 */

var shuttleMessage = '../../../message/pages';   //跳转分包message统一路径
var shuttleMyCenter = '../../../center/pages';  //跳转分包myCenter统一路径
var shuttleIndex = '../../../../pages/index';   //跳转主包统一路径

var shuttle = {
  shuttleMessage,
  shuttleMyCenter,
  shuttleIndex,
}
module.exports = shuttle