/**
 * 主包index配置文件
 */

var indexShuttle = '../../subPackages/shuttle/pages';   //跳转分包shuttle统一路径
var indexMessage = '../../subPackages/message/pages';  //跳转分包message统一路径
var indexCenter = '../../subPackages/center/pages';   //跳转分包center统一路径

var index = {
  indexShuttle,
  indexMessage,
  indexCenter,
}

module.exports = index